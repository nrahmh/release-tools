# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::PublicRelease::CNGImageRelease do
  let(:gitlab_version) { '42.0.0' }
  let(:version) { ReleaseTools::CNGVersion.new("#{gitlab_version}-ee") }
  let(:release) { described_class.new(version) }

  describe '#initialize' do
    it 'converts CE versions to EE versions' do
      version = ReleaseTools::Version.new('42.0.0')
      release = described_class.new(version)

      expect(release.version).to eq('42.0.0-ee')
    end
  end

  describe '#execute' do
    let(:ce_tag) { double(:tag, name: "#{gitlab_version}.0.0-ce") }
    let(:ee_tag) { double(:tag, name: "#{gitlab_version}.0.0-ee") }
    let(:ubi_tag) { double(:tag, name: "#{gitlab_version}.0.0-ubi8") }
    let(:fips_tag) { double(:tag, name: "#{gitlab_version}.0.0-fips") }

    shared_examples 'release executor' do
      it 'runs the release' do
        expect(release).to receive(:create_target_branch)
        expect(release)
          .to receive(:update_component_versions)
          .and_return([ce_tag, ee_tag, ubi_tag, fips_tag])

        expect(release)
          .to receive(:add_release_data_for_tags)
          .with(ce_tag, ee_tag, ubi_tag, fips_tag)

        expect(release).to receive(:notify_slack)

        without_dry_run { release.execute }
      end

      context 'with dry-run mode' do
        it 'skips most of the tasks' do
          expect(release).to receive(:create_target_branch)

          expect(release).not_to receive(:update_component_versions)
          expect(release).not_to receive(:add_release_data_for_tags)
          expect(release).not_to receive(:notify_slack)

          release.execute
        end
      end
    end

    it_behaves_like 'release executor'

    context 'with no FIPS tag' do
      let(:gitlab_version) { '15.0.0' }
      let(:fips_tag) { nil }

      it_behaves_like 'release executor'
    end
  end

  describe '#update_component_versions' do
    let(:ce_data) { double(:ce_data) }
    let(:ee_data) { double(:ee_data) }
    let(:ce_tag) { double(:ce_tag) }
    let(:ee_tag) { double(:ee_tag) }
    let(:ubi_tag) { double(:ubi_tag) }
    let(:fips_tag) { double(:fips_tag) }

    shared_examples 'component updater' do
      it 'updates the component versions' do
        expect(release)
        .to receive(:distribution_component_versions)
        .and_return([ce_data, ee_data])

        expect(release).to receive(:create_ce_tag).with(ce_data).and_return(ce_tag)
        expect(release).to receive(:create_ee_tag).with(ee_data).and_return(ee_tag)
        expect(release).to receive(:create_ubi_tag).and_return(ubi_tag)
        expect(release).to receive(:create_fips_tag).and_return(fips_tag)

        expect(release.update_component_versions).to eq([ce_tag, ee_tag, ubi_tag, fips_tag])
      end
    end

    it_behaves_like 'component updater'

    context 'with no FIPS tag' do
      let(:fips_tag) { nil }

      it_behaves_like 'component updater'
    end
  end

  describe '#create_ce_tag' do
    it 'creates the CE tag' do
      expect(release)
        .to receive(:find_or_create_tag)
        .with('v42.0.0', {})

      release.create_ce_tag({})
    end
  end

  describe '#create_ee_tag' do
    it 'creates the EE tag' do
      expect(release)
        .to receive(:find_or_create_tag)
        .with('v42.0.0-ee', {})

      release.create_ee_tag({})
    end
  end

  describe '#create_ubi_tag' do
    it 'creates the UBI tag' do
      expect(release.client)
        .to receive(:find_or_create_tag)
        .with(
          release.project_path,
          'v42.0.0-ubi8',
          '42-0-stable',
          { message: 'Version v42.0.0-ubi8' }
        )

      release.create_ubi_tag
    end
  end

  describe '#create_fips_tag' do
    it 'creates the FIPS tag' do
      expect(release.client)
        .to receive(:find_or_create_tag)
        .with(
          release.project_path,
          'v42.0.0-fips',
          '42-0-stable',
          { message: 'Version v42.0.0-fips' }
        ).and_return(double(:tag))

      expect(release.create_fips_tag).to be_present
    end

    context 'with GitLab 15.0' do
      let(:gitlab_version) { '15.0.0' }

      it 'does not return a tag' do
        expect(release.client)
          .not_to receive(:find_or_create_tag)

        expect(release.create_fips_tag).to be_nil
      end
    end
  end

  describe '#find_or_create_tag' do
    let(:version) { ReleaseTools::CNGVersion.new('42.0.0') }
    let(:release) { described_class.new(version) }

    context 'when the tag already exists' do
      it 'returns the tag' do
        tag = double(:tag)

        allow(release.client)
          .to receive(:tag)
          .with(release.project_path, { tag: 'foo' })
          .and_return(tag)

        expect(release).not_to receive(:commit_version_files)
        expect(release.find_or_create_tag('foo', {})).to eq(tag)
      end
    end

    context 'when there is a response error' do
      it 'retries the operation' do
        tag = double(:tag)
        raised = false

        allow(release.client).to receive(:tag) do
          if raised
            tag
          else
            raised = true
            raise gitlab_error(:InternalServerError)
          end
        end

        expect(release).not_to receive(:commit_version_files)
        expect(release.find_or_create_tag('foo', {})).to eq(tag)
      end
    end

    context 'when the tag does not exist' do
      it 'updates the variables file and creates a tag' do
        tag = double(:tag)

        allow(release.client)
          .to receive(:tag)
          .with(release.project_path, { tag: 'foo' })
          .and_raise(gitlab_error(:NotFound))

        expect(release)
          .to receive(:commit_version_files)
          .with(
            '42-0-stable',
            { described_class::VARIABLES_FILE => YAML.dump('variables' => {}) }
          )

        expect(release.client)
          .to receive(:create_tag)
          .with(
            release.project_path,
            'foo',
            release.target_branch,
            'Version foo'
          )
          .and_return(tag)

        expect(release.find_or_create_tag('foo', { 'variables' => {} }))
          .to eq(tag)
      end
    end
  end

  describe '#distribution_component_versions' do
    it 'returns the component versions for CE and EE' do
      allow(release)
        .to receive(:component_versions)
        .and_return('FOO' => 'v1.0.0')

      allow(release)
        .to receive(:read_file)
        .with(described_class::VARIABLES_FILE)
        .and_return(YAML.dump('variables' => { 'BLA' => 'master' }))

      ce, ee = release.distribution_component_versions

      expect(ce).to eq(
        'variables' => {
          'GITLAB_VERSION' => 'v42.0.0',
          'GITLAB_REF_SLUG' => 'v42.0.0',
          'GITLAB_ASSETS_TAG' => 'v42-0-0',
          'FOO' => 'v1.0.0',
          'BLA' => 'master'
        }
      )

      expect(ee).to eq(
        'variables' => {
          'GITLAB_VERSION' => 'v42.0.0-ee',
          'GITLAB_REF_SLUG' => 'v42.0.0-ee',
          'GITLAB_ASSETS_TAG' => 'v42-0-0-ee',
          'FOO' => 'v1.0.0',
          'BLA' => 'master'
        }
      )
    end

    it 'respects MAILROOM_VERSION value' do
      allow(release)
        .to receive(:component_versions)
        .and_return('FOO' => 'v1.0.0')

      allow(release)
        .to receive(:read_file)
        .with(described_class::VARIABLES_FILE)
        .and_return(YAML.dump('variables' => { 'MAILROOM_VERSION' => '1.2.3' }))

      ce, ee = release.distribution_component_versions

      expect(ce).to eq(
        'variables' => {
          'GITLAB_VERSION' => 'v42.0.0',
          'GITLAB_REF_SLUG' => 'v42.0.0',
          'GITLAB_ASSETS_TAG' => 'v42-0-0',
          'FOO' => 'v1.0.0',
          'MAILROOM_VERSION' => '1.2.3'
        }
      )

      expect(ee).to eq(
        'variables' => {
          'GITLAB_VERSION' => 'v42.0.0-ee',
          'GITLAB_REF_SLUG' => 'v42.0.0-ee',
          'GITLAB_ASSETS_TAG' => 'v42-0-0-ee',
          'FOO' => 'v1.0.0',
          'MAILROOM_VERSION' => '1.2.3'
        }
      )
    end
  end

  describe '#component_versions' do
    let(:default_components) do
      {
        'GITALY_SERVER_VERSION' => 'v1.0.0',
        'GITLAB_SHELL_VERSION' => 'v2.0.0',
        'GITLAB_ELASTICSEARCH_INDEXER_VERSION' => 'v4.0.0',
        'GITLAB_PAGES_VERSION' => 'v5.0.0',
        'GITLAB_KAS_VERSION' => 'v6.0.0'
      }
    end

    before do
      allow(release)
        .to receive(:read_ee_file)
        .with('Gemfile.lock')
        .and_return(File.read("#{VersionFixture.new.fixture_path}/Gemfile.lock"))

      allow(release)
        .to receive(:read_ee_file)
        .with('GITALY_SERVER_VERSION')
        .and_return('1.0.0')

      allow(release)
        .to receive(:read_ee_file)
        .with('GITLAB_SHELL_VERSION')
        .and_return('2.0.0')

      allow(release)
        .to receive(:read_ee_file)
        .with('GITLAB_ELASTICSEARCH_INDEXER_VERSION')
        .and_return('4.0.0')

      allow(release)
        .to receive(:read_ee_file)
        .with('GITLAB_PAGES_VERSION')
        .and_return('5.0.0')

      allow(release)
        .to receive(:read_ee_file)
        .with('GITLAB_KAS_VERSION')
        .and_return('6.0.0')

      allow(release)
        .to receive(:read_ee_file)
        .with('GITLAB_METRICS_EXPORTER_VERSION')
        .and_return('7.0.0')
    end

    context 'when GitLab version is older than 15.0' do
      let(:version) { ReleaseTools::CNGVersion.new('14.9.0-ee') }

      it 'returns the raw component versions' do
        expect(release.component_versions).to eq(default_components)
      end
    end

    context 'when GitLab version is 15.0 or newer' do
      let(:version) { ReleaseTools::CNGVersion.new('15.0.0-ee') }

      it 'returns the raw component versions' do
        expect(release.component_versions).to eq(
          default_components.merge('GITLAB_METRICS_EXPORTER_VERSION' => 'v7.0.0')
        )
      end
    end

    context 'when GitLab version is 15.11 or newer' do
      let(:version) { ReleaseTools::CNGVersion.new('15.11.0-ee') }
      let(:additional_versions) do
        {
          'GITLAB_METRICS_EXPORTER_VERSION' => 'v7.0.0',
          'MAILROOM_VERSION' => '0.9.1'
        }
      end

      it 'returns the raw component versions' do
        allow(release)
          .to receive(:read_file)
          .with(described_class::VARIABLES_FILE)
          .and_return(YAML.dump('variables' => { 'MAILROOM_VERSION' => '1.2.3' }))

        expect(release.component_versions).to eq(
          default_components.merge(additional_versions)
        )
      end
    end
  end

  describe '#add_release_data_for_tags' do
    let(:ce_tag) { double(:tag, name: "#{gitlab_version}.ce") }
    let(:ee_tag) { double(:tag, name: "#{gitlab_version}.ee") }
    let(:ubi_tag) { double(:tag, name: "#{gitlab_version}.ubi") }
    let(:fips_tag) { double(:tag, name: "#{gitlab_version}.fips") }

    shared_examples 'release tagger' do
      it 'adds the release data for the tags' do
        expect(release)
          .to receive(:add_release_data)
          .with('cng-ce', gitlab_version, ce_tag)

        expect(release)
          .to receive(:add_release_data)
          .with('cng-ee', gitlab_version, ee_tag)

        expect(release)
          .to receive(:add_release_data)
          .with('cng-ee-ubi', gitlab_version, ubi_tag)

        if fips_tag
          expect(release)
            .to receive(:add_release_data)
            .with('cng-ee-fips', gitlab_version, fips_tag)
        else
          expect(release)
            .not_to receive(:add_release_data)
            .with('cng-ee-fips', gitlab_version, fips_tag)
        end

        release.add_release_data_for_tags(ce_tag, ee_tag, ubi_tag, fips_tag)
      end
    end

    it_behaves_like 'release tagger'

    context 'with a nil FIPS tag' do
      let(:fips_tag) { nil }

      it_behaves_like 'release tagger'
    end
  end

  describe '#add_release_data' do
    it 'records the release data' do
      expect(release.release_metadata)
        .to receive(:add_release)
        .with({ name: 'cng', version: '1.2.3', sha: '123abc', ref: 'foo', tag: true })

      release.add_release_data(
        'cng',
        '1.2.3',
        double(:tag, commit: double(:commit, id: '123abc'), name: 'foo')
      )
    end
  end

  describe '#read_ee_file' do
    it 'reads a file from the GitLab EE repository' do
      expect(release)
        .to receive(:read_file)
        .with('README.md', 'gitlab-org/gitlab', '42-0-stable-ee')
        .and_return('foo')

      expect(release.read_ee_file('README.md')).to eq('foo')
    end
  end

  describe '#read_file' do
    it 'reads a file from a branch' do
      expect(release.client)
        .to receive(:file_contents)
        .with('gitlab-org/build/CNG', 'README.md', '42-0-stable')
        .and_return("foo\n")

      expect(release.read_file('README.md')).to eq('foo')
    end

    it 'retries the operation if it fails' do
      raised = false

      allow(release.client).to receive(:file_contents) do
        if raised
          "foo\n"
        else
          raised = true
          raise gitlab_error(:InternalServerError)
        end
      end

      expect(release.read_file('README.md')).to eq('foo')
    end
  end

  describe '#version_string' do
    it 'prefixes a major-minor version' do
      expect(release.version_string('1.2.3')).to eq('v1.2.3')
    end

    it 'avoids double-prefixing a semver version' do
      expect(release.version_string('v1.2.3')).to eq('v1.2.3')
    end

    it 'does not prefix a branch name' do
      expect(release.version_string('master')).to eq('master')
    end

    it 'does not prefix a commit SHA' do
      expect(release.version_string('123abc')).to eq('123abc')
    end
  end

  describe '#project' do
    it 'returns the project' do
      expect(release.project).to eq(ReleaseTools::Project::CNGImage)
    end
  end

  describe '#source_for_target_branch' do
    context 'when a custom commit is specified' do
      it 'returns the commit' do
        release = described_class.new(version, commit: 'foo')

        expect(release.source_for_target_branch).to eq('foo')
      end
    end

    context 'when no custom commit is specified' do
      it 'returns the default branch name' do
        release = described_class.new(version)

        expect(release.source_for_target_branch).to eq('master')
      end
    end
  end
end
