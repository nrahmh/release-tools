# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ReleaseTools::Deployments::BlockerIssue do
  let(:labels) do
    [
      'Deploys-blocked-gprd::4hr',
      'Deploys-blocked-gstg::0.5hr',
      'incident',
      'Incident::Resolved',
      'Service::Kube',
      'Source::IMA::IncidentDeclare',
      'severity::3'
    ]
  end

  let(:issue) do
    create(
      :issue,
      title: 'incident blocker',
      labels: labels
    )
  end

  subject(:blocker) do
    described_class.new(issue)
  end

  describe '#title' do
    it { expect(blocker.title).to eq(issue.title) }
  end

  describe '#url' do
    it { expect(blocker.url).to eq(issue.web_url) }
  end

  describe '#hours_gstg_blocked' do
    context 'with deploy block label' do
      it 'fetches the hours from the gstg label' do
        expect(blocker.hours_gstg_blocked).to eq(0.5)
      end
    end

    context 'with no deploy block label' do
      let(:labels) { ['incident'] }

      it { expect(blocker.hours_gstg_blocked).to eq(0) }
    end
  end

  describe '#hours_gprd_blocked' do
    context 'with deploy block label' do
      it 'fetches the hours from the gprd label' do
        expect(blocker.hours_gprd_blocked).to eq(4)
      end
    end

    context 'with no deploy block label' do
      let(:labels) { ['incident'] }

      it { expect(blocker.hours_gstg_blocked).to eq(0) }
    end
  end
end
