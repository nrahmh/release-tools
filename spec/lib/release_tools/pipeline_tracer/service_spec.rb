# frozen_string_literal: true

require 'spec_helper'
require 'opentelemetry/sdk'
require 'opentelemetry/exporter/otlp'

describe ReleaseTools::PipelineTracer::Service do
  describe '.from_pipeline_url' do
    it 'creates Service instance' do
      url = 'https://ops.gitlab.net/gitlab-org/release/tools/-/pipelines/1590941'

      instance = described_class.from_pipeline_url(url, pipeline_name: 'pipeline 1', trace_depth: 2)

      expect(instance.pipeline.gitlab_instance).to eq('ops.gitlab.net')
      expect(instance.pipeline.id).to eq('1590941')
      expect(instance.pipeline.project).to eq('gitlab-org/release/tools')
      expect(instance.pipeline_name).to eq('pipeline 1')
      expect(instance.trace_depth).to eq(2)
    end
  end

  describe '#execute' do
    subject(:execute) { described_class.new(**attrs).execute }

    let(:pipeline) do
      ReleaseTools::PipelineTracer::Pipeline.new('ops.gitlab.net', 'gitlab-org/release/tools', '1590941')
    end

    let(:attrs) do
      {
        pipeline: pipeline,
        pipeline_name: 'pipeline 1',
        trace_depth: trace_depth
      }
    end

    let(:root_span_attributes) do
      {
        user: "gitlab-release-tools-bot",
        web_url: "https://ops.gitlab.net/gitlab-org/release/tools/-/pipelines/1590941",
        created_at: "2022-12-07T16:21:00.557Z",
        updated_at: "2022-12-07T22:52:01.402Z",
        status: "success",
        started_at: "2022-12-07T16:21:03.514Z",
        finished_at: "2022-12-07T22:52:01.395Z"
      }
    end

    let(:trace_depth) { 2 }
    let(:tracer) { instance_double(OpenTelemetry::Internal::ProxyTracer) }
    let(:root_span) { instance_double(OpenTelemetry::Trace::Span) }
    let(:bridge_jobs) { [] }
    let(:process_jobs) { instance_double(ReleaseTools::PipelineTracer::ProcessJobs, execute: nil) }

    before do
      allow(::OpenTelemetry)
        .to receive(:tracer_provider)
        .and_return(instance_double(OpenTelemetry::Internal::ProxyTracerProvider, tracer: tracer))

      allow(tracer)
        .to receive(:start_span)
        .and_return(root_span)

      allow(ReleaseTools::PipelineTracer::ProcessJobs).to receive(:new).and_return(process_jobs)

      allow(root_span)
        .to receive(:finish)

      allow(pipeline).to receive(:start_time).and_return("2022-12-07T16:21:03.514Z")
      allow(pipeline).to receive(:end_time).and_return("2022-12-07T22:52:01.395Z")
      allow(pipeline).to receive(:root_attributes).and_return(root_span_attributes)

      allow(pipeline).to receive(:bridge_jobs).and_return(Gitlab::PaginatedResponse.new(bridge_jobs))
    end

    it 'creates root span' do
      expect(::OpenTelemetry).to receive(:tracer_provider)

      expect(tracer)
        .to receive(:start_span)
        .with(
          attrs[:pipeline_name],
          {
            kind: :internal,
            start_timestamp: Time.parse("2022-12-07T16:21:03.514Z"),
            attributes: root_span_attributes.stringify_keys
          }
        )

      expect(root_span).to receive(:finish).with({ end_timestamp: Time.parse("2022-12-07T22:52:01.395Z") })
      expect(pipeline).to receive(:bridge_jobs)

      without_dry_run { execute }
    end

    it 'calls ProcessJobs class' do
      expect(ReleaseTools::PipelineTracer::ProcessJobs).to receive(:new).with(tracer, pipeline, { trace_depth: 2 })
      expect(process_jobs).to receive(:execute)

      without_dry_run { execute }
    end

    context 'with bridge jobs' do
      let(:bridge_jobs) do
        [Gitlab::ObjectifiedHash.new({ 'name' => 'deploy:gprd',
           'downstream_pipeline' => {
             "id" => 1_591_456,
             "iid" => 20_732,
             "project_id" => 151,
             "sha" => "9b96150cae4af18398188f4133a5b23adcb6c3ab",
             "ref" => "master",
             "status" => "success",
             "source" => "pipeline",
             "created_at" => "2022-12-07T21:25:22.012Z",
             "updated_at" => "2022-12-07T22:51:24.658Z",
             "web_url" => "https://ops.gitlab.net/gitlab-com/gl-infra/deployer/-/pipelines/1591456"
           } })]
      end

      it 'calls Service' do
        expect(pipeline).to receive(:bridge_jobs)

        expect(described_class)
          .to receive(:from_pipeline_url)
          .with(
            'https://ops.gitlab.net/gitlab-com/gl-infra/deployer/-/pipelines/1591456',
            {
              pipeline_name: 'deploy:gprd bridged downstream pipeline',
              trace_depth: 1
            }
          )
          .and_return(double(execute: nil))

        without_dry_run { execute }
      end

      context 'bridge job has no downstream pipeline' do
        let(:bridge_jobs) do
          [Gitlab::ObjectifiedHash.new({ 'name' => 'deploy:gprd',
           'downstream_pipeline' => nil })]
        end

        it 'skips bridge with no downstream pipeline' do
          expect(pipeline).to receive(:bridge_jobs)
          expect(described_class).not_to receive(:from_pipeline_url)

          without_dry_run { execute }
        end
      end
    end

    context 'with trace_depth less than 1' do
      let(:trace_depth) { 0 }

      it 'does not create sub-spans' do
        expect(pipeline).not_to receive(:bridge_jobs)
        expect(described_class).not_to receive(:from_pipeline_url)

        expect(ReleaseTools::PipelineTracer::ProcessJobs).not_to receive(:new)

        without_dry_run { execute }
      end
    end

    context 'with dry run' do
      it 'does nothing' do
        expect(::OpenTelemetry).not_to receive(:tracer_provider)

        execute
      end
    end
  end
end
