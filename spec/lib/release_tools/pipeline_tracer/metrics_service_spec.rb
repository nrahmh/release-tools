# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::PipelineTracer::MetricsService do
  describe '.from_pipeline_url' do
    it 'creates Service instance' do
      url = 'https://ops.gitlab.net/gitlab-org/release/tools/-/pipelines/1590941'

      instance = described_class.from_pipeline_url(url, { version: 'pipeline 1', depth: 2 })

      pipeline = instance.instance_variable_get(:@pipeline)
      version = instance.instance_variable_get(:@version)
      depth = instance.instance_variable_get(:@depth)

      expect(pipeline.gitlab_instance).to eq('ops.gitlab.net')
      expect(pipeline.id).to eq('1590941')
      expect(pipeline.project).to eq('gitlab-org/release/tools')
      expect(version).to eq('pipeline 1')
      expect(depth).to eq(2)
    end
  end

  describe '#execute' do
    subject(:instance) { described_class.new(pipeline: pipeline, version: version, depth: depth) }

    let(:pipeline) do
      ReleaseTools::PipelineTracer::Pipeline.new('ops.gitlab.net', 'gitlab-org/release/tools', '1590941')
    end

    let(:depth) { 2 }
    let(:version) { '15.10.202303221120-355b04fb8d1.936857331ed' }
    let(:metrics_client) { instance_double(ReleaseTools::Metrics::Client) }

    let(:pipeline_duration_labels) do
      "gitlab-org/release/tools,#{version},#{pipeline_details.status},test-pipeline,#{pipeline.id},#{pipeline_details.web_url},gstg,cny"
    end

    let(:pipeline_details) do
      build(
        :pipeline,
        :success,
        id: pipeline.id,
        name: 'test-pipeline',
        web_url: "https://ops.gitlab.net/gitlab-org/release/tools/-/pipelines/#{pipeline.id}"
      )
    end

    let(:job_duration_labels) do
      "job1,stage1,success,gitlab-org/release/tools,#{version},,,job1,https://example.com/foo/bar/-/jobs/#{job.id},#{job.id},#{pipeline.id}"
    end

    let(:jobs) do
      [
        build(
          :job, :success, name: 'job1'
        ),
        build(
          :job, :running, name: 'job2'
        )
      ]
    end

    let(:bridge_jobs) { [build(:bridge_job, :success, name: 'bridge_job1')] }

    let(:job) do
      ReleaseTools::PipelineTracer::Job.new(jobs[0], ReleaseTools::GitlabOpsClient)
    end

    before do
      allow(pipeline).to receive(:real_time_duration).and_return(200)

      allow(ReleaseTools::GitlabOpsClient).to receive(:pipeline).and_return(pipeline_details)

      allow(ReleaseTools::GitlabOpsClient)
        .to receive(:pipeline_variables)
        .and_return([build(:gitlab_response, key: 'DEPLOY_ENVIRONMENT', value: 'gstg-cny')])

      allow(ReleaseTools::PipelineTracer::Job).to receive(:new).and_call_original
      allow(ReleaseTools::PipelineTracer::Job).to receive(:new).with(jobs[0], ReleaseTools::GitlabOpsClient).and_return(job)
      allow(job).to receive(:real_time_duration).and_return(300)

      allow(ReleaseTools::Metrics::Client).to receive(:new).and_return(metrics_client)

      allow(pipeline)
        .to receive(:jobs)
        .and_return(Gitlab::PaginatedResponse.new(jobs))

      allow(pipeline)
        .to receive(:bridge_jobs)
        .and_return(Gitlab::PaginatedResponse.new(bridge_jobs))

      allow(described_class)
        .to receive(:from_pipeline_url)
        .and_return(double(execute: nil))

      allow(metrics_client).to receive(:set)
    end

    it 'creates metrics for pipeline' do
      expect(metrics_client)
        .to receive(:set)
        .with('deployment_pipeline_duration_seconds', pipeline.real_time_duration, labels: pipeline_duration_labels)

      without_dry_run { instance.execute }
    end

    it 'creates metric for jobs' do
      expect(metrics_client)
        .to receive(:set)
        .with('deployment_job_duration_seconds', job.real_time_duration, labels: job_duration_labels)

      expect(metrics_client)
        .not_to receive(:set)
        .with('deployment_job_duration_seconds', anything, labels: include("job2"))

      without_dry_run { instance.execute }
    end

    it 'creates metric for bridge jobs' do
      expect(described_class)
        .to receive(:from_pipeline_url)
        .with(bridge_jobs[0].downstream_pipeline.web_url, { version: version, depth: 1 })
        .and_return(double(execute: nil))

      without_dry_run { instance.execute }
    end

    context 'with environment in name' do
      let(:job_duration_labels) do
        "#{job.name},stage1,success,gitlab-org/release/tools,#{version},gstg,cny,metrics:deployment_completed:,https://example.com/foo/bar/-/jobs/#{job.id},#{job.id},#{pipeline.id}"
      end

      let(:jobs) do
        [build(:job, :success, name: 'metrics:deployment_completed:gstg-cny'), build(:job, :running, name: 'job2')]
      end

      it 'creates metric for jobs' do
        expect(metrics_client)
          .to receive(:set)
          .with('deployment_job_duration_seconds', job.real_time_duration, labels: job_duration_labels)

        without_dry_run { instance.execute }
      end
    end

    context 'when depth is invalid' do
      let(:depth) { 4 }

      it 'raises error' do
        expect { instance.execute }.to raise_error(described_class::InvalidDepthError, 'Depth must be between 0 and 3')
      end
    end

    context 'when depth is 0' do
      subject(:instance) { described_class.new(pipeline: pipeline, version: version, depth: 0) }

      it 'does not create job duration metric' do
        expect(metrics_client)
          .to receive(:set)
          .with('deployment_pipeline_duration_seconds', pipeline.real_time_duration, labels: pipeline_duration_labels)

        expect(metrics_client).not_to receive(:set).with('deployment_job_duration_seconds', any_args)

        expect(described_class).not_to receive(:from_pipeline_url)

        without_dry_run { instance.execute }
      end
    end

    context 'with dry run' do
      it 'does nothing' do
        expect(metrics_client).not_to receive(:set)

        instance.execute
      end
    end
  end
end
