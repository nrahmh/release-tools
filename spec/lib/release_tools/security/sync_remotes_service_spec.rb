# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::SyncRemotesService do
  describe '#execute' do
    let(:other_args) { ['master', { create_mr_on_failure: true }] }
    let(:service) { described_class.new }

    before do
      enable_feature(:pages_managed_versioning)
    end

    it 'syncs master branch for every project' do
      expect(service).to receive(:sync_branches)
        .with(ReleaseTools::Project::GitlabEe, *other_args)

      expect(service).to receive(:sync_branches)
        .with(ReleaseTools::Project::GitlabCe, *other_args)

      expect(service).to receive(:sync_branches)
        .with(ReleaseTools::Project::OmnibusGitlab, *other_args)

      expect(service).to receive(:sync_branches)
        .with(ReleaseTools::Project::Gitaly, *other_args)

      expect(service).to receive(:sync_branches)
        .with(ReleaseTools::Project::HelmGitlab, *other_args)

      expect(service).to receive(:sync_branches)
        .with(ReleaseTools::Project::GitlabOperator, *other_args)

      expect(service).to receive(:sync_branches)
        .with(ReleaseTools::Project::GitlabPages, *other_args)

      service.execute
    end

    context 'with pages_managed_versioning feature flag disabled' do
      before do
        disable_feature(:pages_managed_versioning)
      end

      it 'does not sync GitlabPages branches' do
        expect(service).not_to receive(:sync_branches)
          .with(ReleaseTools::Project::GitlabPages, *other_args)

        service.execute
      end
    end
  end
end
