# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Services::SyncRefsHelper do
  describe '#sync_branches' do
    before do
      foo_class = Class.new do
        include ::SemanticLogger::Loggable
        include ReleaseTools::Services::SyncRefsHelper
      end

      stub_const('FooClass', foo_class)
    end

    let(:foo_class) { FooClass.new }

    let(:project) { ReleaseTools::Project::GitlabEe }

    let(:fake_repo) do
      instance_double(ReleaseTools::RemoteRepository).as_null_object
    end

    context 'with invalid remotes' do
      it 'does nothing' do
        stub_const("ReleaseTools::Project::GitlabEe::REMOTES", {})

        expect(ReleaseTools::RemoteRepository).not_to receive(:get)

        foo_class.sync_branches(project, 'branch')
      end
    end

    context 'with dry_run' do
      it 'does nothing' do
        branch = '1-2-stable-ee'

        expect(ReleaseTools::RemoteRepository).not_to receive(:get)

        expect(fake_repo).not_to receive(:merge)

        foo_class.sync_branches(project, branch)
      end
    end

    context 'with a successful merge' do
      it 'merges branch and pushes' do
        branch = '1-2-stable-ee'

        successful_merge = double(status: double(success?: true))

        expect(ReleaseTools::RemoteRepository).to receive(:get)
          .with(
            a_hash_including(project::REMOTES),
            a_hash_including(branch: branch)
          ).and_return(fake_repo)

        expect(fake_repo).to receive(:merge)
          .with("dev/#{branch}", { no_ff: true })
          .and_return(successful_merge)

        expect(fake_repo).to receive(:push_to_all_remotes).with(branch).and_return([true])

        without_dry_run do
          foo_class.sync_branches(project, branch)
        end
      end
    end

    context 'with a failed merge' do
      let(:failed_merge) { double(status: double(success?: false), output: 'output') }
      let(:branch) { '1-2-stable-ee' }
      let(:source_branch) { "dev-#{branch}" }

      before do
        allow(foo_class.logger).to receive(:info).and_call_original

        allow(ReleaseTools::RemoteRepository)
          .to receive(:get)
          .with(project::REMOTES, a_hash_including(branch: branch))
          .and_return(fake_repo)

        allow(fake_repo).to receive(:merge).and_return(failed_merge)
      end

      it 'logs a fatal message with the output when create_mr_on_failure is false' do
        expect(fake_repo).to receive(:merge).and_return(failed_merge)
        expect(fake_repo).not_to receive(:push_to_all_remotes)

        expect(foo_class.logger).to receive(:fatal)
          .with(anything, a_hash_including(output: 'output'))

        without_dry_run do
          foo_class.sync_branches(project, branch)
        end
      end

      context 'when create_mr_on_failure argument is true' do
        context 'when create_mr_on_failure feature flag is off' do
          it 'logs a fatal message with the output' do
            expect(fake_repo).to receive(:merge).and_return(failed_merge)
            expect(fake_repo).not_to receive(:push_to_all_remotes)

            expect(foo_class.logger).to receive(:fatal)
              .with(anything, a_hash_including(output: 'output'))

            without_dry_run do
              foo_class.sync_branches(project, branch, create_mr_on_failure: true)
            end
          end
        end

        context 'when create_mr_on_failure feature flag is on' do
          let(:merge_request_double) { double(create: double(web_url: 'merge request URL')) }

          before do
            enable_feature(:create_mr_on_failure)

            allow(ReleaseTools::SyncRemotesMergeRequest)
              .to receive(:new)
              .with({ project: project, target_branch: branch })
              .and_return(merge_request_double)
          end

          it 'creates a merge request' do
            expect(fake_repo).not_to receive(:push_to_all_remotes)
            expect(foo_class.logger).to receive(:info)
              .with('Failed to sync branch, creating merge request instead', a_hash_including(output: 'output'))

            expect(ReleaseTools::SyncRemotesMergeRequest)
              .to receive(:new)
              .with({ project: project, target_branch: branch })

            without_dry_run do
              foo_class.sync_branches(project, branch, create_mr_on_failure: true)
            end
          end

          it 'logs fatal message when creation of merge request fails' do
            allow(merge_request_double).to receive(:create).and_return(false)

            expect(fake_repo).not_to receive(:push_to_all_remotes)

            expect(merge_request_double).to receive(:create)

            expect(foo_class.logger).to receive(:fatal).with(
              'Could not create merge request',
              { project: project, target_branch: branch }
            )

            without_dry_run do
              foo_class.sync_branches(project, branch, create_mr_on_failure: true)
            end
          end
        end
      end
    end

    context 'with a failed push' do
      let(:branch) { '1-2-stable-ee' }
      let(:source_branch) { "dev-#{branch}" }
      let(:merge_request_double) { double(create: build(:merge_request)) }

      before do
        allow(foo_class.logger).to receive(:info).and_call_original

        allow(ReleaseTools::RemoteRepository)
          .to receive(:get)
          .with(project::REMOTES, a_hash_including(branch: branch))
          .and_return(fake_repo)

        allow(fake_repo).to receive(:merge).and_return(double(status: double(success?: true), output: 'output'))
        allow(fake_repo).to receive(:push_to_all_remotes).and_return([true, false])

        allow(ReleaseTools::SyncRemotesMergeRequest)
          .to receive(:new)
          .with({ project: project, target_branch: branch })
          .and_return(merge_request_double)

        enable_feature(:create_mr_on_failure)
      end

      it 'creates a merge request' do
        expect(foo_class.logger).to receive(:info)
          .with('Push failed, creating merge request instead', anything)

        expect(ReleaseTools::SyncRemotesMergeRequest)
          .to receive(:new)
          .with({ project: project, target_branch: branch })

        without_dry_run do
          foo_class.sync_branches(project, branch, { create_mr_on_failure: true })
        end
      end

      context 'with create_mr_on_failure feature flag disabled' do
        before do
          disable_feature(:create_mr_on_failure)
        end

        it 'does not create a merge request' do
          expect(ReleaseTools::SyncRemotesMergeRequest).not_to receive(:new)

          without_dry_run do
            foo_class.sync_branches(project, branch, { create_mr_on_failure: true })
          end
        end
      end

      context 'with create_mr_on_failure option set to false' do
        it 'does not create a merge request' do
          expect(ReleaseTools::SyncRemotesMergeRequest).not_to receive(:new)

          without_dry_run do
            foo_class.sync_branches(project, branch)
          end
        end
      end
    end
  end
end
