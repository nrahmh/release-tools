# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Slack::CoordinatedPipelineNotification do
  let(:deploy_version) { '14.3.202108261121-1c87faa073d.2cec58b7eb5' }
  let(:deployer_url) { 'https://test.com/deployer/-/pipelines/123' }
  let(:environment) { 'gstg' }
  let(:slack_msg_response) { { 'ok' => true, 'channel' => 'channel', 'ts' => 'thread_ts' } }

  let(:pipeline) do
    create(
      :pipeline,
      :running,
      web_url: deployer_url,
      created_at: '2021-09-14T15:15:23.923Z'
    )
  end

  subject(:notifier) do
    described_class.new(
      deploy_version: deploy_version,
      pipeline: pipeline,
      environment: environment
    )
  end

  describe '#execute' do
    let(:local_time) { Time.utc(2021, 9, 14, 15, 55, 0) }

    let(:context_elements) do
      [
        { text: ':clock1: 2021-09-14 15:55 UTC', type: 'mrkdwn' },
        { text: ':sentry: <https://sentry.gitlab.net/gitlab/gitlabcom/releases/1c87faa073d/|View Sentry>', type: 'mrkdwn' }
      ]
    end

    before do
      allow(ReleaseTools::Slack::Message)
        .to receive(:post)
        .and_return(slack_msg_response)

      allow(ReleaseTools::Slack::Search)
          .to receive(:query)
          .with("gstg started #{deploy_version}", { channel: 'announcements' })
          .and_return([])

      allow(ReleaseTools::Slack::CoordinatedPipelineDiffsNotification)
        .to receive(:new)
        .with(
          {
            deploy_version: deploy_version,
            environment: environment,
            thread_ts: slack_msg_response['ts']
          }
        )
        .and_return(double(execute: { 'ok' => true }))
    end

    it 'sends a slack message' do
      block_content =
        ":building_construction: :ci_running: *gstg* <#{deployer_url}|started> `#{deploy_version}`"

      expect(ReleaseTools::Slack::Message)
        .to receive(:post)
        .with(
          {
            channel: ReleaseTools::Slack::ANNOUNCEMENTS,
            message: "gstg started 14.3.202108261121-1c87faa073d.2cec58b7eb5",
            blocks: slack_mrkdwn_block(text: block_content, context_elements: context_elements)
          }
        )

      without_dry_run do
        Timecop.freeze(local_time) do
          notifier.execute
        end
      end
    end

    it 'posts diff notification' do
      diff_notification_response = { 'ok' => true }
      expect(ReleaseTools::Slack::CoordinatedPipelineDiffsNotification)
        .to receive(:new)
        .with(
          {
            deploy_version: deploy_version,
            environment: environment,
            thread_ts: 'thread_ts'
          }
        )
        .and_return(double(execute: diff_notification_response))

      without_dry_run do
        expect(notifier.execute).to eq(diff_notification_response)
      end
    end

    context 'when threading to started notification' do
      let(:block_content) { ":building_construction: :ci_passing: *gstg* <#{deployer_url}|finished> `#{deploy_version}`" }

      let(:pipeline) do
        create(
          :pipeline,
          :success,
          web_url: deployer_url,
          created_at: '2021-09-14T15:15:23.923Z'
        )
      end

      let(:elements) do
        context_elements.concat(
          [{ type: 'mrkdwn', text: ":timer_clock: 39 minutes" }]
        )
      end

      let(:response) do
        [{
          "ts" => "ts",
          "text" => "",
          "blocks" => [
            {
              "type" => "section",
              "block_id" => "block ID",
              "text" => {
                "type" => "mrkdwn",
                "text" => ":building_construction: :ci_running: *gstg* <https://ops.gitlab.net/gitlab-com/gl-infra/deployer/-/pipelines/1123900|started> `#{deploy_version}`",
                "verbatim" => false
              }
            }
          ],
          "permalink" => "https://gitlab.slack.com/archives/C0139MAV672/p1647572855397269"
        }, {
          "ts" => "ts2",
          "text" => "",
          "blocks" => [
            {
              "type" => "section",
              "block_id" => "block ID",
              "text" => {
                "type" => "mrkdwn",
                "text" => ":building_construction: :ci_running: *gstg* <https://ops.gitlab.net/gitlab-com/gl-infra/deployer/-/pipelines/1123900|started> `14.3.202107211320-sha1.sha2`",
                "verbatim" => false
              }
            }
          ],
          "permalink" => "permalink URL 1"
        }, {
          "ts" => "ts3",
          "text" => "",
          "permalink" => "permalink URL 2"
        }]
      end

      it 'sends a slack message to deployment started notification thread' do
        expect(ReleaseTools::Slack::Search)
          .to receive(:query)
          .with("gstg started #{deploy_version}", { channel: 'announcements' })
          .and_return(response)

        expect(ReleaseTools::Slack::Message)
          .to receive(:post)
          .with(
            {
              channel: ReleaseTools::Slack::ANNOUNCEMENTS,
              message: "gstg finished #{deploy_version}",
              blocks: slack_mrkdwn_block(text: block_content, context_elements: elements),
              additional_options: { thread_ts: 'ts', reply_broadcast: true }
            }
          )

        without_dry_run do
          Timecop.freeze(local_time) do
            notifier.execute
          end
        end
      end

      it 'does not thread message to deployment started notification when no search results' do
        expect(ReleaseTools::Slack::Search)
          .to receive(:query)
          .with("gstg started #{deploy_version}", { channel: 'announcements' })
          .and_return([])

        expect(ReleaseTools::Slack::Message)
          .to receive(:post)
          .with(
            {
              channel: ReleaseTools::Slack::ANNOUNCEMENTS,
              message: "gstg finished #{deploy_version}",
              blocks: slack_mrkdwn_block(text: block_content, context_elements: elements)
            }
          )

        without_dry_run do
          Timecop.freeze(local_time) do
            notifier.execute
          end
        end
      end

      context 'when notification is for start of deployment' do
        let(:block_content) { ":building_construction: :ci_running: *gstg* <#{deployer_url}|started> `#{deploy_version}`" }
        let(:elements) { context_elements }

        let(:pipeline) do
          create(
            :pipeline,
            :running,
            web_url: deployer_url,
            created_at: '2021-09-14T15:15:23.923Z'
          )
        end

        it 'does not send slack message to deployment started notification thread' do
          expect(ReleaseTools::Slack::Search)
            .not_to receive(:query)

          expect(ReleaseTools::Slack::Message)
            .to receive(:post)
            .with(
              {
                channel: ReleaseTools::Slack::ANNOUNCEMENTS,
                message: "gstg started #{deploy_version}",
                blocks: slack_mrkdwn_block(text: block_content, context_elements: context_elements)
              }
            )

          without_dry_run do
            Timecop.freeze(local_time) do
              notifier.execute
            end
          end
        end
      end
    end

    context 'when notification is not for start' do
      before do
        allow(pipeline).to receive(:status).and_return('success')
      end

      it 'does not post diff notification' do
        expect(ReleaseTools::Slack::CoordinatedPipelineDiffsNotification)
          .not_to receive(:new)

        without_dry_run do
          expect(notifier.execute).to eq(slack_msg_response)
        end
      end
    end

    context 'with no pipeline' do
      around do |ex|
        ClimateControl.modify(CI_PIPELINE_URL: pipeline_url, &ex)
      end

      let(:pipeline) { nil }
      let(:pipeline_url) { 'http://example.com' }

      it 'sends a slack message' do
        block_content =
          ":building_construction: :warning: *gstg* Downstream pipeline couldn't be found, check if `deploy:gstg` job in <#{pipeline_url}|coordinated pipeline> has started. `#{deploy_version}`"

        expect(ReleaseTools::Slack::Message)
          .to receive(:post)
          .with(
            {
              channel: ReleaseTools::Slack::ANNOUNCEMENTS,
              message: "gstg unknown 14.3.202108261121-1c87faa073d.2cec58b7eb5",
              blocks: slack_mrkdwn_block(text: block_content, context_elements: context_elements)
            }
          )

        without_dry_run do
          Timecop.freeze(local_time) do
            notifier.execute
          end
        end
      end
    end

    context 'with a success pipeline' do
      let(:pipeline) do
        create(
          :pipeline,
          :success,
          web_url: deployer_url,
          created_at: '2021-09-14T15:15:23.923Z'
        )
      end

      it 'includes the correct status and the wall time' do
        block_content =
          ":building_construction: :ci_passing: *gstg* <#{deployer_url}|finished> `#{deploy_version}`"

        elements = context_elements.concat(
          [{ type: 'mrkdwn', text: ":timer_clock: 39 minutes" }]
        )

        expect(ReleaseTools::Slack::Message)
          .to receive(:post)
          .with(
            {
              channel: ReleaseTools::Slack::ANNOUNCEMENTS,
              message: "gstg finished 14.3.202108261121-1c87faa073d.2cec58b7eb5",
              blocks: slack_mrkdwn_block(text: block_content, context_elements: elements)
            }
          )

        without_dry_run do
          Timecop.freeze(local_time) do
            notifier.execute
          end
        end
      end
    end

    context 'with a failed pipeline' do
      let(:pipeline) do
        create(
          :pipeline,
          :failed,
          web_url: deployer_url,
          created_at: '2021-09-14T15:15:23.923Z'
        )
      end

      it 'includes the correct status and the wall time' do
        block_content =
          ":building_construction: :ci_failing: *gstg* <#{deployer_url}|failed> `#{deploy_version}`"

        elements = context_elements.concat(
          [{ type: 'mrkdwn', text: ":timer_clock: 39 minutes" }]
        )

        expect(ReleaseTools::Slack::Message)
          .to receive(:post)
          .with(
            {
              channel: ReleaseTools::Slack::ANNOUNCEMENTS,
              message: "gstg failed 14.3.202108261121-1c87faa073d.2cec58b7eb5",
              blocks: slack_mrkdwn_block(text: block_content, context_elements: elements)
            }
          )

        without_dry_run do
          Timecop.freeze(local_time) do
            notifier.execute
          end
        end
      end
    end

    context 'with a long deployment' do
      let(:pipeline) do
        create(
          :pipeline,
          :success,
          web_url: deployer_url,
          created_at: '2021-09-14T13:13:23.923Z'
        )
      end

      it 'includes the correct duration' do
        block_content =
          ":building_construction: :ci_passing: *gstg* <#{deployer_url}|finished> `#{deploy_version}`"

        elements = context_elements.concat(
          [{ type: 'mrkdwn', text: ":timer_clock: 2 hours" }]
        )

        expect(ReleaseTools::Slack::Message)
          .to receive(:post)
          .with(
            {
              channel: ReleaseTools::Slack::ANNOUNCEMENTS,
              message: "gstg finished 14.3.202108261121-1c87faa073d.2cec58b7eb5",
              blocks: slack_mrkdwn_block(text: block_content, context_elements: elements)
            }
          )

        without_dry_run do
          Timecop.freeze(local_time) do
            notifier.execute
          end
        end
      end
    end

    context 'with a different environment' do
      let(:environment) { 'gprd' }

      it 'displays the environment and specific emoji' do
        block_content =
          ":party-tanuki: :ci_running: *gprd* <#{deployer_url}|started> `#{deploy_version}`"

        expect(ReleaseTools::Slack::Message)
          .to receive(:post)
          .with(
            {
              channel: ReleaseTools::Slack::ANNOUNCEMENTS,
              message: "gprd started #{deploy_version}",
              blocks: slack_mrkdwn_block(text: block_content, context_elements: context_elements)
            }
          )

        without_dry_run do
          Timecop.freeze(local_time) do
            notifier.execute
          end
        end
      end
    end

    context 'with dry run mode' do
      let(:slack_msg_response) { {} }

      it 'returns empty hash' do
        expect(ReleaseTools::Slack::CoordinatedPipelineDiffsNotification)
          .to receive(:new)
          .with(
            {
              deploy_version: deploy_version,
              environment: environment,
              thread_ts: nil
            }
          )
          .and_return(double(execute: {}))

        expect(notifier.execute).to eq({})
      end
    end
  end
end
