# frozen_string_literal: true

require 'spec_helper'
require 'release_tools/tasks'

describe ReleaseTools::Tasks::Metrics::DeploymentMetrics::MergeRequestLeadTime do
  subject(:service) { described_class.new }

  let(:package_version) { "15.10.202303060320-d244fd30a63.41707614427" }
  let(:metrics) { instance_double(ReleaseTools::Metrics::Client) }
  let(:merged_at) { Time.parse("2023-03-10T12:34:03Z") }
  let(:updated_at) { Time.parse("2023-03-10T12:34:13Z") }
  let(:product_version) { build(:product_version) }
  let(:gitlab_sha) { product_version[ReleaseTools::Project::GitlabEe.metadata_project_name].sha }
  let(:canonical_commits) { [build(:commit, id: gitlab_sha)] }
  let(:security_commits) { [build(:commit, id: gitlab_sha)] }
  let(:security_deployments) do
    [
      build(
        :deployment,
        id: 12_345,
        environment: double(name: "gprd"),
        sha: gitlab_sha,
        updated_at: updated_at.iso8601
      )
    ]
  end

  let(:canonical_deployments) do
    [
      build(
        :deployment,
        environment: double(name: "gprd"),
        sha: gitlab_sha,
        updated_at: updated_at.iso8601
      )
    ]
  end

  let(:mr) do
    [
      build(
        :merge_request,
        merged_at: merged_at.iso8601,
        iid: 12_345,
        title: "Mocked title"
      )
    ]
  end

  describe '#execute' do
    before do
      allow(ReleaseTools::ProductVersion).to receive(:from_package_version).with(package_version).and_return(product_version)

      allow(ReleaseTools::GitlabClient).to receive(:deployments)
        .with(ReleaseTools::Project::GitlabEe, "gprd", { status: "success" }).and_return(canonical_deployments)

      allow(ReleaseTools::GitlabClient).to receive(:deployments)
        .with(ReleaseTools::Project::GitlabEe.security_path, "gprd", { status: "success" }).and_return(security_deployments)

      allow(ReleaseTools::GitlabClient).to receive(:commits)
        .with(ReleaseTools::Project::GitlabEe.path, { ref_name: ReleaseTools::Project::GitlabEe.default_branch, per_page: 100 })
        .and_return(Gitlab::PaginatedResponse.new(canonical_commits))

      allow(ReleaseTools::GitlabClient).to receive(:commits)
        .with(ReleaseTools::Project::GitlabEe.auto_deploy_path, { ref_name: gitlab_sha })
        .and_return(Gitlab::PaginatedResponse.new(security_commits))

      allow(ReleaseTools::GitlabClient).to receive(:deployed_merge_requests)
        .with(ReleaseTools::Project::GitlabEe, security_deployments[0].id)
        .and_return([])

      allow(ReleaseTools::GitlabClient).to receive(:deployed_merge_requests)
        .with(ReleaseTools::Project::GitlabEe, canonical_deployments[0].id)
        .and_return(mr)

      allow(ReleaseTools::Metrics::Client).to receive(:new).and_return(metrics)
    end

    it 'records MR lead time metric' do
      expect(metrics).to receive(:set)
        .with(
          "deployment_merge_request_lead_time_seconds",
          10,
          { labels: "gprd,main,#{canonical_deployments[0].iid},#{mr[0].iid},15.10.202303060320-d244fd30a63.41707614427" }
        )

      without_dry_run do
        service.execute("gprd", "15.10.202303060320-d244fd30a63.41707614427")
      end
    end

    it 'does not records MR lead time metric during dry_run' do
      expect(metrics).not_to receive(:set)
      service.execute("gprd", "15.10.202303060320-d244fd30a63.41707614427")
    end

    context "when canonical sha is different" do
      let(:canonical_commits) { [build(:commit), build(:commit, id: canonical_sha)] }
      let(:security_commits) { [build(:commit, id: gitlab_sha), build(:commit, id: canonical_sha)] }

      let(:canonical_sha) { "abc123" }

      let(:canonical_deployments) do
        [
          build(
            :deployment,
            id: 54_321,
            environment: double(name: "gprd"),
            sha: canonical_sha,
            updated_at: updated_at.iso8601
          )
        ]
      end

      let(:security_mr) { build(:merge_request, merged_at: merged_at.iso8601, title: 'Security MR') }

      before do
        allow(ReleaseTools::GitlabClient).to receive(:deployed_merge_requests)
          .with(ReleaseTools::Project::GitlabEe, security_deployments[0].id)
          .and_return([security_mr])
      end

      it 'records MR lead time metric with different sha' do
        expect(metrics).to receive(:set)
          .with(
            "deployment_merge_request_lead_time_seconds",
            10,
            { labels: "gprd,main,#{canonical_deployments[0].id},#{mr[0].iid},15.10.202303060320-d244fd30a63.41707614427" }
          )
        expect(metrics).to receive(:set)
          .with(
            "deployment_merge_request_lead_time_seconds",
            10,
            { labels: "gprd,main,#{security_deployments[0].id},#{security_mr.iid},15.10.202303060320-d244fd30a63.41707614427" }
          )

        without_dry_run do
          service.execute("gprd", "15.10.202303060320-d244fd30a63.41707614427")
        end
      end
    end
  end
end
