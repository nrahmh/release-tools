# frozen_string_literal: true

require 'spec_helper'
require 'release_tools/tasks'

RSpec.describe ReleaseTools::Tasks::Release::Prepare do
  describe '#execute' do
    subject(:execute) { instance.execute }

    let(:instance) { described_class.new(version) }
    let(:issue) { instance_spy(ReleaseTools::Tasks::Release::Issue, execute: nil) }

    before do
      allow(ReleaseTools::Tasks::Release::Issue)
        .to receive(:new)
        .with(version)
        .and_return(issue)
    end

    shared_examples 'a patch release' do
      let(:patch_issue) { ReleaseTools::PatchRelease::Issue.new(version: version) }

      before do
        allow(issue).to receive(:release_issue).and_return(patch_issue)
        allow(instance).to receive(:create_or_show_merge_request)
        allow(patch_issue).to receive(:add_blog_mr_to_description)
      end

      it 'creates issue' do
        expect(ReleaseTools::Tasks::Release::Issue)
          .to receive(:new)
          .with(version)

        expect(issue).to receive(:execute)

        execute
      end

      it 'creates patch blog merge request' do
        expect(instance)
          .to receive(:create_or_show_merge_request)
          .with(instance_of(ReleaseTools::PatchRelease::BlogMergeRequest))

        execute
      end

      it 'adds MR URL to patch issue description' do
        expect(patch_issue)
          .to receive(:add_blog_mr_to_description)
          .with('test.gitlab.com')

        execute
      end
    end

    context 'with patch version' do
      let(:version) { ReleaseTools::Version.new('14.1.9') }

      it_behaves_like 'a patch release'
    end

    context 'with nil version' do
      let(:version) { nil }
      let(:coordinator) { instance_double(ReleaseTools::PatchRelease::Coordinator, merge_requests: []) }

      before do
        allow(ReleaseTools::PatchRelease::Coordinator).to receive(:new).and_return(coordinator)
      end

      it_behaves_like 'a patch release'
    end
  end
end
