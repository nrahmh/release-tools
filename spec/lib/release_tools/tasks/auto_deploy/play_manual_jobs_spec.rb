# frozen_string_literal: true

require 'spec_helper'
require 'release_tools/tasks'

describe ReleaseTools::Tasks::AutoDeploy::PlayManualJobs do
  let(:current_pipeline_id) { 20 }
  let(:trace_job_name) { 'metrics:trace-pipeline' }
  let(:current_pipeline_jobs) { [build(:job, :manual, name: trace_job_name)] }
  let(:pipelines) { [] }

  describe '#execute' do
    subject(:execute) { described_class.new(current_pipeline_id).execute }

    before do
      allow(ReleaseTools::GitlabOpsClient)
        .to receive(:pipeline_jobs)
        .with(ReleaseTools::Project::ReleaseTools, current_pipeline_id, { per_page: 70 })
        .and_return(Gitlab::PaginatedResponse.new(current_pipeline_jobs))

      allow(ReleaseTools::GitlabOpsClient)
        .to receive(:job_play)
        .with(ReleaseTools::Project::ReleaseTools, current_pipeline_jobs[0].id)

      allow(ReleaseTools::GitlabOpsClient)
        .to receive(:pipelines)
        .with(ReleaseTools::Project::ReleaseTools, { scope: 'tags', per_page: 15 })
        .and_return(pipelines)
    end

    it 'plays trace job of current pipeline' do
      expect(ReleaseTools::GitlabOpsClient)
        .to receive(:job_play)
        .with(ReleaseTools::Project::ReleaseTools, current_pipeline_jobs[0].id)

      without_dry_run { execute }
    end

    context 'with pipelines' do
      let(:previous_pipeline1) { build(:pipeline, :failed, id: 18) }
      let(:newer_pipeline) { build(:pipeline, :success, id: 21) }
      let(:previous_pipeline_running) { build(:pipeline, :running, id: 19) }
      let(:pipelines) { [newer_pipeline, previous_pipeline_running, previous_pipeline1] }
      let(:trace_job) { build(:job, :manual, name: trace_job_name) }

      before do
        allow(ReleaseTools::GitlabOpsClient)
          .to receive(:pipeline_jobs)
          .with(ReleaseTools::Project::ReleaseTools, previous_pipeline1.id, { per_page: 70 })
          .and_return(Gitlab::PaginatedResponse.new([trace_job]))

        allow(ReleaseTools::GitlabOpsClient)
          .to receive(:job_play)
          .with(ReleaseTools::Project::ReleaseTools, trace_job.id)
      end

      it 'plays trace-pipeline jobs of previous pipelines' do
        expect(ReleaseTools::GitlabOpsClient)
          .to receive(:job_play)
          .with(ReleaseTools::Project::ReleaseTools, trace_job.id)

        without_dry_run { execute }
      end

      it 'ignores newer pipelines' do
        expect(ReleaseTools::GitlabOpsClient)
          .not_to receive(:pipeline_jobs)
          .with(ReleaseTools::Project::ReleaseTools, newer_pipeline.id, anything)

        without_dry_run { execute }
      end

      it 'ignores previous pipelines that have not completed' do
        expect(ReleaseTools::GitlabOpsClient)
          .not_to receive(:pipeline_jobs)
          .with(ReleaseTools::Project::ReleaseTools, previous_pipeline_running.id, anything)

        without_dry_run { execute }
      end

      context 'with a non manual trace-pipeline job' do
        let(:trace_job) { build(:job, :success, name: 'trace-pipeline') }

        it 'does not attempt to play the trace-pipeline job' do
          expect(ReleaseTools::GitlabOpsClient)
            .not_to receive(:job_play)
            .with(ReleaseTools::Project::ReleaseTools, trace_job.id)

          without_dry_run { execute }
        end
      end
    end

    context 'with dry run' do
      it 'does not play the trace jobs' do
        expect(ReleaseTools::GitlabOpsClient).not_to receive(:job_play)

        execute
      end
    end
  end
end
