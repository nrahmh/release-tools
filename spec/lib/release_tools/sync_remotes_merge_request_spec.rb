# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::SyncRemotesMergeRequest do
  let(:project) { ReleaseTools::Project::GitlabEe }
  let(:target_branch) { 'branch' }
  let(:source_branch) { 'sync-canonical-with-security-changes' }
  let(:merge_request) { described_class.new(project: project, target_branch: target_branch) }

  describe '#labels' do
    it 'sets correct labels on MR' do
      expect(merge_request.labels).to contain_exactly('team::Delivery', 'pipeline:skip-undercoverage')
    end
  end

  describe '#title' do
    it 'has an informative title' do
      expect(merge_request.title).to eq("Syncing #{target_branch} into #{project.metadata_project_name}")
    end
  end

  describe '#assignee_ids' do
    it 'returns current release manager ids' do
      schedule = instance_spy(
        ReleaseTools::ReleaseManagers::Schedule,
        active_release_managers: [double(id: 1), double(id: 2)]
      )
      allow(ReleaseTools::ReleaseManagers::Schedule).to receive(:new).and_return(schedule)

      expect(merge_request.assignee_ids).to eq([1, 2])
    end

    it 'logs fatal message when current release managers are unknown' do
      allow(ReleaseTools::ReleaseManagers::Schedule)
        .to receive(:new)
        .and_raise(ReleaseTools::ReleaseManagers::Schedule::VersionNotFoundError, 'error')

      expect(merge_request.logger).to receive(:fatal)
        .with('Could not find active release managers')

      expect(merge_request.assignee_ids).to be_nil
    end
  end

  describe '#source_branch' do
    it 'returns source branch name' do
      expect(merge_request.source_branch).to eq(source_branch)
    end
  end

  describe '#create' do
    let(:fake_repo) do
      instance_spy(ReleaseTools::RemoteRepository).as_null_object
    end

    before do
      allow(ReleaseTools::RemoteRepository)
        .to receive(:get)
        .with(project::REMOTES, a_hash_including(branch: target_branch))
        .and_return(fake_repo)

      allow(fake_repo).to receive(:fetch).with(target_branch, remote: :dev)
      allow(fake_repo).to receive(:checkout_new_branch).with(source_branch, base: "dev/#{target_branch}")

      allow(fake_repo).to receive(:push).with(:canonical, source_branch).and_return(true)
    end

    it 'creates source branch' do
      expect(ReleaseTools::RemoteRepository)
        .to receive(:get)
        .with(project::REMOTES, a_hash_including(branch: target_branch))

      expect(fake_repo).to receive(:fetch).with(target_branch, a_hash_including(remote: :dev))
      expect(fake_repo).to receive(:checkout_new_branch).with(source_branch, { base: "dev/#{target_branch}" })

      expect(fake_repo).to receive(:push).with(:canonical, source_branch)

      expect(ReleaseTools::GitlabClient).to receive(:create_merge_request).with(merge_request, project)

      without_dry_run do
        merge_request.create
      end
    end

    context 'when CannotCheckoutBranchError is raised' do
      before do
        allow(fake_repo)
          .to receive(:checkout_new_branch)
          .and_raise(ReleaseTools::RemoteRepository::CannotCheckoutBranchError, 'error')
      end

      it 'logs fatal message' do
        expect(ReleaseTools::RemoteRepository)
          .to receive(:get)
          .with(project::REMOTES, a_hash_including(branch: target_branch))

        expect(fake_repo).to receive(:fetch).with(target_branch, a_hash_including(remote: :dev))
        expect(fake_repo).to receive(:checkout_new_branch).with(source_branch, { base: "dev/#{target_branch}" })

        expect(merge_request.logger).to receive(:fatal).with(
          'Could not create new branch from dev remote',
          { error_message: 'error', branch: target_branch, project: project }
        )

        expect(fake_repo).not_to receive(:push).with(:canonical, source_branch)

        expect(ReleaseTools::GitlabClient).not_to receive(:create_merge_request)

        without_dry_run do
          merge_request.create
        end
      end
    end
  end
end
