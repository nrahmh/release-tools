# frozen_string_literal: true

module ReleaseTools
  module Metrics
    # Interface to delivery-metrics Pushgateway
    class Client
      include ::SemanticLogger::Loggable

      GATEWAY = ENV.fetch('DELIVERY_METRICS_URL', nil)
      TOKEN = ENV.fetch('DELIVERY_METRICS_TOKEN', nil)

      def initialize
        @client = HTTP.headers('X-Private-Token': TOKEN)
      end

      def observe(metric, value, labels: "")
        post(metric, __method__, value, labels)
      end

      def inc(metric, labels: "")
        post(metric, __method__, nil, labels)
      end

      def set(metric, value, labels: "")
        post(metric, __method__, value, labels)
      end

      def reset(metric)
        response = @client.delete(url_for(metric))

        return if response.status.success?

        logger.error(
          'Metric reset failed',
          name: metric,
          status: response.status.code.to_s,
          message: response.body.to_s
        )
      end

      private

      def post(metric, action, value, labels)
        response = @client.post(
          "#{url_for(metric)}/#{action}",
          form: {
            value: value,
            labels: labels
          }.compact
        )

        return if response.status.success?

        logger.error(
          'Recording metric failed',
          name: metric,
          action: action,
          status: response.status.code.to_s,
          message: response.body.to_s
        )
      end

      def url_for(metric)
        "#{GATEWAY}/api/#{metric}"
      end
    end
  end
end
