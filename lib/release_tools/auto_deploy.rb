# frozen_string_literal: true

require 'release_tools/auto_deploy/naming'
require 'release_tools/auto_deploy/builder/cng_image'
require 'release_tools/auto_deploy/builder/omnibus'
require 'release_tools/auto_deploy/pipeline'
require 'release_tools/auto_deploy/tag'
require 'release_tools/auto_deploy/tagger/release_metadata_tracking'
require 'release_tools/auto_deploy/tagger/cng_image'
require 'release_tools/auto_deploy/tagger/coordinator'
require 'release_tools/auto_deploy/tagger/helm'
require 'release_tools/auto_deploy/tagger/omnibus'
require 'release_tools/auto_deploy/version'
require 'release_tools/auto_deploy/wait_for_package'
require 'release_tools/auto_deploy/wait_for_pipeline'
require 'release_tools/auto_deploy/cleanup'

# TODO: Relocate -- https://gitlab.com/gitlab-org/release-tools/-/issues/491
require 'release_tools/auto_deploy_branch'

module ReleaseTools
  module AutoDeploy
    # list of projects with auto-deploy branches
    PROJECTS = [
      Project::GitlabEe,
      Project::OmnibusGitlab,
      Project::CNGImage,
      Project::HelmGitlab
    ].freeze

    module_function

    # Determines if auto-deploys are happening in "coordinated" mode
    def coordinator_pipeline?
      ENV['CI_COMMIT_TAG'].present? || ENV['AUTO_DEPLOY_TAG'].present?
    end
  end
end
