# frozen_string_literal: true

module ReleaseTools
  module Slack
    class Search
      include ::SemanticLogger::Loggable

      API_URL = 'https://slack.com/api/search.messages'

      NoCredentialsError = Class.new(StandardError)
      CouldNotSearchError = Class.new(StandardError)

      def self.query(query, from_user: nil, channel: nil)
        raise NoCredentialsError unless slack_token.present?

        query += " in:#{channel}" if channel
        query += " from:@#{from_user}" if from_user

        params = { query: query }

        logger.trace(__method__, params)

        if Feature.enabled?(:slack_down)
          logger.warn("Not attempting to call Slack search API because FF 'slack_down' is enabled", params: params)
          return []
        end

        response = client.post(API_URL, form: params)

        unless response.status.success?
          logger.warn('Slack search.messages API call was not successful', code: response.code, response_body: response.body, request_params: params)
          raise CouldNotSearchError.new("#{response.code} #{response.reason} #{response.body}")
        end

        body = response.parse
        return body.dig('messages', 'matches') if body['ok']

        logger.warn('Slack search.messages API call returned error', response_body: body, request_params: params)
        raise CouldNotSearchError.new("#{response.code} #{response.reason} #{body}")
      end

      def self.client
        @client ||= HTTP.auth("Bearer #{slack_token}")
      end

      def self.slack_token
        ENV.fetch('SLACK_APP_USER_TOKEN', '')
      end
    end
  end
end
