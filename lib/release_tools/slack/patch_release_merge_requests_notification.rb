# frozen_string_literal: true

module ReleaseTools
  module Slack
    class PatchReleaseMergeRequestsNotification < Webhook
      include ::SemanticLogger::Loggable

      def self.webhook_url
        ENV.fetch('SLACK_CHATOPS_URL')
      end

      def self.notify(patch_versions)
        content = '```'

        patch_versions.each do |version|
          next unless version[:pressure].positive?

          content += "\n### #{version[:version]}\n\n"

          version[:merge_requests].each do |_, project_merge_requests|
            project_merge_requests.each do |merge_request|
              content += "* [#{merge_request['title']}](#{merge_request['web_url']})\n"
            end
          end
        end

        content += '```'

        blocks = ::Slack::BlockKit.blocks
        text = '*Merge requests merged into upcoming patch versions*'
        text = '*No merge requests are awaiting patch release*' if content == '``````'

        logger.info(text)
        logger.info(content.gsub('```', ''))

        blocks.section { |section| section.mrkdwn(text: text) }
        blocks.section { |section| section.mrkdwn(text: content) } unless content == '``````'

        fire_hook(blocks: blocks.as_json, channel: ENV.fetch('CHAT_CHANNEL', F_UPCOMING_RELEASE))
      end
    end
  end
end
