# frozen_string_literal: true

module ReleaseTools
  module AutoDeploy
    module QaNotifier
      include ReleaseTools::AutoDeploy::Pipeline

      def execute
        qa_pipelines = find_qa_pipelines

        if qa_pipelines.present?
          web_urls = qa_pipelines.map(&:downstream_pipeline).map(&:web_url)

          logger.info('QA pipelines found', quality_pipelines: web_urls.map(&:to_s))

          send_slack_notification(qa_pipelines)
        else
          logger.fatal('QA pipelines not found')
        end
      end

      private

      attr_reader :pipeline_id, :deploy_version, :environment, :qa_smoke_jobs

      def find_qa_pipelines
        logger.info('Fetching qa bridges', environment: environment, deploy_version: deploy_version, pipeline_id: pipeline_id)

        bridges
          .select { |job| job.status == 'failed' && qa_smoke_jobs.include?(job.name) }
      rescue MissingPipelineError
        []
      end

      def bridges
        pipeline_bridges = Retriable.with_context(:pipeline_created) do
          logger.debug('Finding QA bridges for pipeline', pipeline_id: pipeline_id)

          ReleaseTools::GitlabOpsClient.pipeline_bridges(Project::ReleaseTools, pipeline_id)
        end

        raise MissingPipelineError if pipeline_bridges.empty?

        pipeline_bridges
      end

      def send_slack_notification(qa_pipelines)
        options = {
          deploy_version: deploy_version,
          pipelines: qa_pipelines,
          environment: environment
        }

        ReleaseTools::Slack::QaNotification.new(**options).execute
      end
    end
  end
end
