# frozen_string_literal: true

module ReleaseTools
  module AutoDeploy
    module CoordinatedPipeline
      class Notifier
        include ReleaseTools::AutoDeploy::Pipeline
        include ::SemanticLogger::Loggable

        # pipeline_id    - Pipeline id from release/tools in Ops
        # deploy_version - Deployer package
        # environment    - gstg-ref, gstg-cny, gstg, gprd-cny, gprd
        # slack_channel  - Slack channel that should receive this notification.
        #                  Will use default Slack channel if nil.
        def initialize(pipeline_id:, deploy_version:, environment:, slack_channel: nil)
          @pipeline_id    = pipeline_id
          @deploy_version = deploy_version
          @environment    = environment
          @slack_channel  = slack_channel
        end

        def execute
          logger.debug("Executing notifier", pipeline_id: pipeline_id, deploy_version: deploy_version, environment: environment, slack_channel: slack_channel)

          pipeline = find_downstream_pipeline

          if pipeline
            logger.info('Downstream pipeline found', deployer_url: pipeline.web_url)
          else
            logger.fatal('Downstream pipeline not found')
          end

          send_slack_notification(pipeline)
        end

        private

        attr_reader :pipeline_id, :deploy_version, :environment, :slack_channel

        def find_downstream_pipeline
          logger.info('Fetching downstream pipeline', environment: environment, deploy_version: deploy_version, pipeline_id: pipeline_id)

          Retriable.with_context(:pipeline_created) do
            # The bridges API can return bridge objects that do not contain a downstream_pipeline object,
            # so raise an error to force a retry if that happens.
            bridge.downstream_pipeline or raise MissingPipelineError
          end
        rescue MissingPipelineError
          nil
        end

        def bridge
          logger.debug('Finding bridges for pipeline', pipeline_id: pipeline_id)

          bridge =
            ReleaseTools::GitlabOpsClient
              .pipeline_bridges(Project::ReleaseTools, pipeline_id)
              .find { |job| job.name == "deploy:#{environment}" }

          bridge or raise MissingPipelineError
        end

        def send_slack_notification(deployer_pipeline)
          args = {
            deploy_version: deploy_version,
            pipeline: deployer_pipeline,
            environment: environment
          }
          args[:slack_channel] = slack_channel if slack_channel

          ReleaseTools::Slack::CoordinatedPipelineNotification.new(**args).execute
        end
      end
    end
  end
end
