# frozen_string_literal: true

module ReleaseTools
  module Deployments
    class BlockerIssue
      def initialize(data)
        @data = data
      end

      def title
        data.title
      end

      def root_cause
        ''
      end

      def url
        data.web_url
      end

      def hours_gstg_blocked
        hrs_blocked(environment: 'gstg')
      end

      def hours_gprd_blocked
        hrs_blocked(environment: 'gprd')
      end

      attr_reader :data

      private

      def hrs_blocked(environment:)
        data
          .labels
          .grep(/Deploys-blocked-#{environment}/)
          .first&.gsub(/[^0-9.]/, '')
          .to_f
      end
    end
  end
end
