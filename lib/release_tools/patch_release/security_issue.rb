# frozen_string_literal: true

module ReleaseTools
  module PatchRelease
    class SecurityIssue < Issue
      def title
        if regular?
          "Security patch release: #{versions_title}"
        else
          "Critical security patch release: #{versions_title}"
        end
      end

      def confidential?
        true
      end

      def labels
        "#{super},security"
      end

      def critical?
        ReleaseTools::SharedStatus.critical_security_release?
      end

      def regular?
        !critical?
      end

      def version
        ReleaseTools::Versions.latest(versions, 1).first
      end

      def versions
        patch_release_coordinator.versions
      end

      def informative_title
        if regular?
          'GitLab Security Release'
        else
          'GitLab Critical Security Release'
        end
      end

      def blog_post_merge_request
        # This overrides the method defined in PatchRelease::Issue
        # to do nothing since we do not have a blog merge request class for security issues.
      end

      def add_blog_mr_to_description
        # This overrides the method defined in PatchRelease::Issue
        # to do nothing since we have not automated the blog merge request for security issues.
      end

      protected

      def template_path
        File.expand_path('../../../templates/security_patch.md.erb', __dir__)
      end
    end
  end
end
