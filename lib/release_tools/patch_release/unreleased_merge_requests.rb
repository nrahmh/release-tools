# frozen_string_literal: true

module ReleaseTools
  module PatchRelease
    # Returns unreleased merge requests for projects under Managed Versioning
    class UnreleasedMergeRequests
      include ::SemanticLogger::Loggable

      # version - A ReleaseTools::Version instance
      # project - A ReleaseTools::Project using Managed Versioning
      def initialize(project, version)
        @project = project
        @version = version
      end

      def execute
        merge_requests = commits.map do |commit|
          Retriable.with_context(:api) do
            client.commit_merge_requests(project, sha: commit['id']).first
          end
        end.compact

        logger.info('Fetched unreleased merge requests',
                    project: project,
                    version: version,
                    count: merge_requests.size)

        # remove duplicates when there are multiple commits for the same MR
        merge_requests.uniq do |merge_request|
          merge_request['id']
        end
      end

      def total_pressure
        execute.count
      end

      private

      attr_reader :project, :version

      def commits
        @commits ||= UnreleasedCommits.new(version, project).execute
      end

      def client
        ReleaseTools::GitlabClient
      end
    end
  end
end
