# frozen_string_literal: true

module ReleaseTools
  class SyncRemotesMergeRequest < MergeRequest
    include ::SemanticLogger::Loggable

    def labels
      %w[team::Delivery pipeline:skip-undercoverage]
    end

    def title
      "Syncing #{target_branch} into #{project.metadata_project_name}"
    end

    def assignee_ids
      release_managers&.collect(&:id)
    end

    def source_branch
      'sync-canonical-with-security-changes'
    end

    def create
      return unless create_source_branch!

      super
    end

    protected

    def template_path
      File.expand_path('../../templates/sync_remotes_merge_request.md.erb', __dir__)
    end

    def create_source_branch!
      repository = RemoteRepository.get(project::REMOTES, global_depth: 50, branch: target_branch)

      repository.fetch(target_branch, remote: :dev)
      repository.checkout_new_branch(source_branch, base: "dev/#{target_branch}")

      repository.push(:canonical, source_branch)
    rescue RemoteRepository::CannotCheckoutBranchError => ex
      logger.fatal('Could not create new branch from dev remote', error_message: ex.message, branch: target_branch, project: project)
      false
    end

    def release_managers
      ReleaseManagers::Schedule.new.active_release_managers
    rescue ReleaseManagers::Schedule::VersionNotFoundError
      logger.fatal('Could not find active release managers')
      nil
    end
  end
end
