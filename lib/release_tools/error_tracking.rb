# frozen_string_literal: true

module ReleaseTools
  module ErrorTracking
    module_function

    def with_exception_captured(&block)
      ::Raven.capture do
        ::Sentry.with_exception_captured(&block)
      end
    end

    def capture_exception(ex)
      ::Raven.capture_exception(ex)
      ::Sentry.capture_exception(ex)
    end
  end
end
