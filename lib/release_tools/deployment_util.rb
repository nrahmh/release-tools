# frozen_string_literal: true

module ReleaseTools
  module DeploymentUtil
    module_function

    # Find the first commit that exists in the Security auto-deploy branch and
    # the Canonical default branch
    #
    # The commit being deployed in the auto-deploy branch is usually something
    # that only exists in that branch, and thus can't be used for a Canonical
    # deployment.
    def auto_deploy_intersection(project, sha)
      canonical = GitlabClient
        .commits(project.path, { ref_name: project.default_branch, per_page: 100 })
        .paginate_with_limit(300)
        .collect(&:id)

      GitlabClient.commits(project.auto_deploy_path, { ref_name: sha }).paginate_with_limit(300) do |commit|
        return commit.id if canonical.include?(commit.id)
      end

      logger.warn(
        'Failed to find auto-deploy intersection',
        project: project.path,
        sha: sha
      )

      nil
    rescue Gitlab::Error::Error => ex
      logger.warn(
        'Failed to find auto-deploy intersection',
        project: project.path,
        sha: sha,
        error: ex.message
      )

      nil
    end
  end
end
