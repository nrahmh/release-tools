# frozen_string_literal: true

module ReleaseTools
  module Tasks
    module Release
      class Prepare
        include ReleaseTools::Tasks::Helper

        attr_reader :version

        def initialize(version)
          @version = version
        end

        def execute
          issue = Issue.new(version)
          issue.execute

          prepare_blog_post(issue)
        end

        private

        def prepare_blog_post(issue)
          return if version&.monthly?

          blog_post_mr = prepare_patch_blog_post_merge_request(issue.release_issue)
          issue.release_issue.add_blog_mr_to_description(blog_post_mr.web_url)
        end

        def prepare_patch_blog_post_merge_request(patch_issue)
          if dry_run?
            # rubocop:disable Style/OpenStructUse
            OpenStruct.new(
              description: create_or_show_merge_request(patch_issue.blog_post_merge_request),
              web_url: 'test.gitlab.com'
            )
            # rubocop:enable Style/OpenStructUse
          else
            create_or_show_merge_request(patch_issue.blog_post_merge_request)
          end
        end
      end
    end
  end
end
