# frozen_string_literal: true

module ReleaseTools
  module Tasks
    module AutoDeploy
      class Timer
        include ::SemanticLogger::Loggable

        def execute
          logger.info('Auto-deploy timer', auto_deploy_time: auto_deploy_time?, no_auto_deploy_branch: no_auto_deploy_branch?)

          prepare if auto_deploy_time? && no_auto_deploy_branch?

          pick
          tag
        end

        def schedule
          @schedule ||= schedule_env.split(',', 24).map do |e|
            Integer(e).tap do |hour|
              raise ArgumentError, "#{e} is not a valid hour of the day" if hour.negative? || hour > 23
            end
          end
        end

        def auto_deploy_time?
          return @auto_deploy_time if defined? @auto_deploy_time

          now = Time.now.utc

          @auto_deploy_time = now.on_weekday? && schedule.include?(now.hour)
        end

        def no_auto_deploy_branch?
          return @no_auto_deploy_branch if defined? @no_auto_deploy_branch

          @no_auto_deploy_branch = GitlabClient.find_branch(::ReleaseTools::AutoDeploy::Naming.branch, Project::GitlabEe.auto_deploy_path).nil?
        end

        private

        def schedule_env
          ENV.fetch('AUTO_DEPLOY_SCHEDULE') do |name|
            raise "Missing schedule definition in #{name}"
          end
        end

        def prepare
          ReleaseTools::TraceSection.collapse('Auto-deploy prepare', icon: '🏗️') do
            results = Prepare.new.execute
            gitlab_response = results.find { |r| r.project == ReleaseTools::Project::GitlabEe.security_path }.response
            gitlab_commit = gitlab_response.commit.id
            logger.info("Identifed gitlab passing build", commit: gitlab_commit, branch: gitlab_response.name)

            # TODO: Tag depends on global state extracted by a CI variable that is changed by Prepare.
            #  when running on the same job we need to alter ENV
            ENV[ReleaseTools::Services::AutoDeployBranchService::CI_VAR_AUTO_DEPLOY] = gitlab_response.name
          end
        end

        def tag
          ReleaseTools::TraceSection.collapse('Auto-deploy tag', icon: '🏷️') do
            Tag.new.execute
          end
        end

        def pick
          ReleaseTools::TraceSection.collapse('Auto-deploy pick', icon: '🍒') do
            ::ReleaseTools::AutoDeploy::PROJECTS.each { |project| pick_project(project) }
          end
        end

        def pick_project(project)
          branch_name = ReleaseTools::AutoDeployBranch.current_name
          ReleaseTools.logger.info(
            'Picking into auto-deploy branch',
            project: project.auto_deploy_path,
            target: branch_name
          )

          ReleaseTools::CherryPick::AutoDeployService
            .new(project, branch_name)
            .execute
        end
      end
    end
  end
end
