# frozen_string_literal: true

module ReleaseTools
  module Tasks
    module AutoDeploy
      class Tag
        include ::SemanticLogger::Loggable

        attr_reader :branch, :version

        def initialize
          @branch = ReleaseTools::AutoDeployBranch.current
          @version = ReleaseTools::AutoDeploy::Tag.current
        end

        def metadata
          # TODO(nolith): this will only work if the coordinator pipeline is
          #   running for the first time on that tag.
          #   A retry or a run with $AUTO_DEPLOY_TAG may result in wrong metadata
          @metadata ||= prepopulate_metadata
        end

        def execute
          logger.info('Preparing for tagging', branch: branch.to_s, version: version)

          # NOTE: do not rely on deploy_version before calling build_omnibus and build_cng
          find_passing_build

          omnibus_changes = build_omnibus
          cng_changes = build_cng
          tag_coordinator if omnibus_changes || cng_changes

          return unless ReleaseTools::AutoDeploy.coordinator_pipeline?

          store_deploy_version
          sentry_release
          upload_metadata

          notify
        end

        def notify
          return unless ReleaseTools::AutoDeploy.coordinator_pipeline?

          logger.info('Notifying the coordinated pipeline creation')

          ReleaseTools::Slack::CoordinatedPipelineTagNotification
            .new(deploy_version: deploy_version, auto_deploy_branch: branch, tag_version: version)
            .execute
        end

        def find_passing_build
          logger.info('Searching for commit with passing build', branch: branch.to_s)
          commit
        end

        def build_omnibus
          logger.info('Building Omnibus', commit: commit.id)
          ReleaseTools::AutoDeploy::Builder::Omnibus
            .new(branch, commit.id, metadata)
            .execute
        end

        def build_cng
          logger.info('Building CNG', commit: commit.id)
          ReleaseTools::AutoDeploy::Builder::CNGImage
            .new(branch, commit.id, metadata)
            .execute
        end

        def tag_coordinator
          ReleaseTools::AutoDeploy::Tagger::Coordinator.new.tag!
        end

        def sentry_release
          ReleaseTools::Deployments::SentryTracker.new.release(commit.id)
        end

        def upload_metadata
          ReleaseTools::ReleaseMetadataUploader.new.upload(version, metadata, auto_deploy: true)
        end

        def store_deploy_version
          return if SharedStatus.dry_run?

          logger.info('Storing deploy version', deploy_version: deploy_version)

          File.write('deploy_vars.env', "DEPLOY_VERSION=#{deploy_version}")
        end

        private

        def prepopulate_metadata
          klass = ReleaseTools::ReleaseMetadata

          return klass.new unless ReleaseTools::AutoDeploy.coordinator_pipeline?

          product_version = ProductVersion.last_auto_deploy
          metadata = product_version.metadata

          klass.new.tap do |instance|
            metadata['releases']&.each do |name, data|
              instance.add_release(name: name, **data)
            end
          end
        end

        def commit
          @commit ||= ReleaseTools::PassingBuild.new(branch.to_s).execute
        end

        def deploy_version
          ReleaseTools::AutoDeploy::Version.new(metadata.releases['omnibus-gitlab-ee'].ref).to_package
        end
      end
    end
  end
end
