# frozen_string_literal: true

module ReleaseTools
  module Tasks
    module Metrics
      # Publish metrics related to deployment start time
      class TrackDeploymentStarted
        def execute
          client.inc("deployment_started_total", labels: environment)
          client.inc("deployment_can_rollback_total", labels: environment) if can_rollback?

          client.set("deployment_started", 1, labels: labels)
        end

        private

        def client
          @client ||= ReleaseTools::Metrics::Client.new
        end

        def environment
          ENV.fetch("DEPLOY_ENVIRONMENT")
        end

        def deploy_version
          ENV.fetch("DEPLOY_VERSION")
        end

        def labels
          "#{environment},#{deploy_version}"
        end

        def can_rollback?
          Rollback::CompareService.new(
            current: deploy_version,
            environment: environment
          ).execute.safe?
        rescue ArgumentError
          false
        end
      end
    end
  end
end
