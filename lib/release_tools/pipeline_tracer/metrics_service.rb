# frozen_string_literal: true

module ReleaseTools
  module PipelineTracer
    class MetricsService
      include ::SemanticLogger::Loggable

      MAX_DEPTH = 3

      InvalidDepthError = Class.new(ArgumentError)

      def self.from_pipeline_url(pipeline_url, version:, depth: 2)
        pipeline = Pipeline.from_url(pipeline_url)

        new(
          pipeline: pipeline,
          version: version,
          depth: depth
        )
      end

      # @param [PipelineTracer::Pipeline] pipeline
      # @param [string] version is the DEPLOY_VERSION.
      # @param [integer] depth can be any integer >= 0. It is the depth to which metrics should be generated for this
      #         pipeline. If depth is 1, metrics will not be generated for jobs in downstream pipelines.
      # @return [PipelineTracer::MetricsService]
      def initialize(pipeline:, version:, depth: 2)
        @pipeline = pipeline
        @version = version
        @depth = depth
      end

      def execute
        logger.info('Gathering duration metrics for pipeline', pipeline: pipeline.url)

        raise InvalidDepthError, "Depth must be between 0 and #{MAX_DEPTH}" unless depth.between?(0, MAX_DEPTH)

        return if SharedStatus.dry_run?

        metrics_client.set('deployment_pipeline_duration_seconds', pipeline.real_time_duration, labels: pipeline_duration_labels)

        return if depth < 1

        process_pipeline_jobs
        process_downstream_pipelines
      end

      private

      attr_reader :pipeline, :version, :depth

      def process_pipeline_jobs
        pipeline.jobs.each_page do |page|
          page.each do |job_attributes|
            job = Job.new(job_attributes, pipeline.client)
            next unless job.completed?

            metrics_client.set('deployment_job_duration_seconds', job.real_time_duration, labels: job_duration_labels(job))
          end
        end
      end

      def process_downstream_pipelines
        pipeline.bridge_jobs.each_page do |page|
          page.each do |bridge|
            next unless bridge.downstream_pipeline

            self.class
              .from_pipeline_url(
                bridge.downstream_pipeline.web_url,
                version: version,
                depth: depth - 1
              )
              .execute
          end
        end
      end

      def job_duration_labels(job)
        env = job.environment_from_name
        "#{job.name},#{job.stage},#{job.status},#{pipeline.project},#{version},#{target_env(env)},#{target_stage(env)},#{short_job_name(job)},#{job.web_url},#{job.id},#{pipeline.details.id}"
      end

      def target_env(environment)
        parse_environment(environment)[0]
      end

      def target_stage(environment)
        parse_environment(environment)[1]
      end

      def parse_environment(environment)
        return [] unless environment

        return [environment, 'main'] unless environment.end_with?('-cny')

        [environment.delete_suffix('-cny'), 'cny']
      end

      def short_job_name(job)
        job.name_without_environment
      end

      def pipeline_duration_labels
        env = pipeline.deploy_environment
        "#{pipeline.project},#{version},#{pipeline.details.status},#{pipeline.details.name},#{pipeline.details.id},#{pipeline.details.web_url},#{target_env(env)},#{target_stage(env)}"
      end

      def metrics_client
        @metrics_client ||= Metrics::Client.new
      end
    end
  end
end
