# frozen_string_literal: true

module ReleaseTools
  module Security
    module IssueTable
      class Builder
        include ::SemanticLogger::Loggable

        def initialize(release_manager_comments:, issues:)
          @release_manager_comments = release_manager_comments
          @issues = issues
        end

        def generate
          ERB
            .new(File.read(template_path), trim_mode: '-') # Omit blank lines when using `<% -%>`
            .result(binding)
        end

        private

        attr_reader :release_manager_comments, :issues

        def security_issue_rows
          security_issues
            .map { |issue| generate_table_row(issue) }
            .join("\n")
        end

        def security_issues
          # Keep issues where the master MR has been merged at the top of the table
          issues
            .sort_by { |issue| [issue.default_merge_request_merged? ? 0 : 1, issue.project_id, issue.iid] }
        end

        def generate_table_row(issue)
          "| #{issue_url_and_severity(issue)} | " \
            "#{':white_check_mark:' if issue.default_merge_request_merged?} | " \
            "#{':white_check_mark:' if issue.default_merge_request_deployed?} | " \
            "#{':white_check_mark:' if issue.backports_merged?} | " \
            "#{generate_bot_comment(issue)} | " \
            "#{release_manager_comments[issue.web_url]} |"
        end

        def issue_url_and_severity(issue)
          [
            issue.web_url,
            severity_label(issue)
          ].compact.join(' - ')
        end

        def severity_label(issue)
          label = issue.labels.grep(/^severity::\d$/)&.first
          return unless label

          "~#{label}"
        end

        def generate_bot_comment(issue)
          if !issue.processed? && !issue.ready_to_be_processed?
            return issue.pending_reason
          end

          return unless issue.mwps_set_on_default_merge_request?

          "MWPS set on default branch MR: #{issue.merge_request_targeting_default_branch.web_url}"
        end

        def template_path
          File.expand_path('../../../../templates/security_issues_table.md.erb', __dir__)
        end
      end
    end
  end
end
