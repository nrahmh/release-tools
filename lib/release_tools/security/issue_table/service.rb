# frozen_string_literal: true

module ReleaseTools
  module Security
    # Create a table summarising the status of all security issues as a comment on the security release issue.
    module IssueTable
      class Service
        include ::SemanticLogger::Loggable

        NOTE_HEADER = '## Security issues'

        def initialize(client)
          @client = client
          @release_manager_comments ||= {}
        end

        def execute
          parse_existing_table

          table_str = generate_security_issue_table

          post_issue_note(table_str)
        end

        private

        def parse_existing_table
          return unless existing_note

          # Remove the comment header, so that we are left with only the table itself
          lines = existing_note.body
                               .delete_prefix(NOTE_HEADER)
                               .strip
                               .split("\n")

          # Skip the first 2 lines which are the table header
          lines[2..].each do |line|
            # Break out of the loop once we go past the table lines
            break unless line.include?('|')

            columns = line
                        .delete_prefix('|')
                        .delete_suffix('|')
                        .split('|')

            # First column is in this format "https://example.com/foo/bar/-/issues/1 - ~severity::2"
            issue_web_url = columns.first.split(' - ').first.strip

            @release_manager_comments[issue_web_url] = columns.last.strip
          end
        end

        def generate_security_issue_table
          security_issues = issue_crawler.upcoming_security_issues_and_merge_requests
          table = Builder
                  .new(
                    release_manager_comments: @release_manager_comments,
                    issues: security_issues
                  )
                  .generate

          logger.info('Table of security issues', comment_string: table)
          puts table unless SharedStatus.rspec?

          table
        end

        def post_issue_note(table)
          if existing_note
            logger.info(
              "Editing security release table issue comment",
              existing_note: "#{release_issue.web_url}#note_#{existing_note.id}"
            )
          else
            logger.info("Creating security release table issue comment", issue: release_issue.web_url)
          end

          return if SharedStatus.dry_run?

          Retriable.with_context(:api) do
            if existing_note
              GitlabClient.edit_issue_note(
                release_issue.project_id,
                issue_iid: release_issue.iid,
                note_id: existing_note.id,
                body: table
              )
            else
              GitlabClient.create_issue_note(
                release_issue.project_id,
                issue: release_issue,
                body: table
              )
            end
          end
        end

        def existing_note
          @existing_note ||=
            Retriable.with_context(:api) do
              GitlabClient.issue_notes(release_issue.project_id, issue_iid: release_issue.iid).auto_paginate.detect do |note|
                note.body.include?(NOTE_HEADER) &&
                  note.author.username == Security::Client::RELEASE_TOOLS_BOT_USERNAME
              end
            end
        end

        def issue_crawler
          @issue_crawler ||= Security::IssueCrawler.new
        end

        def release_issue
          issue_crawler.release_issue
        end
      end
    end
  end
end
