# frozen_string_literal: true

module ReleaseTools
  module Security
    class SyncRemotesService
      include ::SemanticLogger::Loggable
      include ReleaseTools::Services::SyncRefsHelper

      # Syncs default branch across all remotes
      def initialize
        @projects = [
          ReleaseTools::Project::GitlabEe,
          ReleaseTools::Project::GitlabCe,
          ReleaseTools::Project::OmnibusGitlab,
          ReleaseTools::Project::Gitaly,
          ReleaseTools::Project::HelmGitlab,
          ReleaseTools::Project::GitlabOperator,
          ReleaseTools::Project::GitlabPages
        ]
      end

      def execute
        @projects.each do |project|
          next if Feature.disabled?(:pages_managed_versioning) && project == ReleaseTools::Project::GitlabPages

          branches = [project.default_branch]

          logger.info('Syncing branches', project: project, branches: branches)
          sync_branches(project, *branches, create_mr_on_failure: true)
        end
      end

      private

      def global_depth
        200
      end
    end
  end
end
