# frozen_string_literal: true

require 'release_tools/tasks/helper'

require 'release_tools/tasks/auto_deploy/baking_time'
require 'release_tools/tasks/auto_deploy/check_package'
require 'release_tools/tasks/auto_deploy/check_production'
require 'release_tools/tasks/auto_deploy/deploy_trigger'
require 'release_tools/tasks/auto_deploy/play_manual_jobs'
require 'release_tools/tasks/auto_deploy/prepare'
require 'release_tools/tasks/auto_deploy/promotion_checks'
require 'release_tools/tasks/auto_deploy/rollback_check'
require 'release_tools/tasks/auto_deploy/tag'
require 'release_tools/tasks/auto_deploy/timer'
require 'release_tools/tasks/auto_deploy/validate_pipeline'

require 'release_tools/tasks/components/update_gitaly'

require 'release_tools/tasks/metrics/deployment_metrics/merge_request_lead_time'
require 'release_tools/tasks/metrics/track_deployment_completed'
require 'release_tools/tasks/metrics/track_deployment_failed_atleast_once'
require 'release_tools/tasks/metrics/track_deployment_started'

require 'release_tools/tasks/production_check/chatops'
require 'release_tools/tasks/production_check/deployment_step'
require 'release_tools/tasks/production_check/incident'

require 'release_tools/tasks/release/issue'
require 'release_tools/tasks/release/prepare'
