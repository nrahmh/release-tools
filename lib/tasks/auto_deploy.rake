# frozen_string_literal: true

namespace :auto_deploy do
  desc "Prepare for auto-deploy by creating branches from the latest green commit on gitlab and omnibus-gitlab"
  task :prepare do
    if ReleaseTools::Feature.enabled?(:auto_deploy_single_timer)
      puts 'Auto-deploy single timer enabled. Nothing to do here'
      next
    end

    ReleaseTools::Tasks::AutoDeploy::Prepare.new.execute
  end

  desc 'Pick commits into the auto deploy branches'
  task :pick do
    if ReleaseTools::Feature.enabled?(:auto_deploy_single_timer)
      puts 'Auto-deploy single timer enabled. Nothing to do here'
      next
    end

    pick = lambda do |project, branch_name|
      ReleaseTools.logger.info(
        'Picking into auto-deploy branch',
        project: project.auto_deploy_path,
        target: branch_name
      )

      ReleaseTools::CherryPick::AutoDeployService
        .new(project, branch_name)
        .execute
    end

    branch_name = ReleaseTools::AutoDeployBranch.current_name

    ReleaseTools::AutoDeploy::PROJECTS.each do |project|
      pick[project, branch_name]
    end
  end

  desc "Tag the auto-deploy branches from the latest passing builds"
  task :tag do
    if ReleaseTools::Feature.enabled?(:auto_deploy_single_timer) && !ReleaseTools::AutoDeploy.coordinator_pipeline?
      puts 'Auto-deploy single timer enabled. Nothing to do here'
      next
    end

    ReleaseTools::Tasks::AutoDeploy::Tag.new.execute
  end

  desc "Verify if the gitlab pipeline is green for this package"
  task :validate_pipeline do
    ReleaseTools::Tasks::AutoDeploy::ValidatePipeline.new.execute
  end

  desc "Lifecyle timer"
  task :timer do
    if ReleaseTools::Feature.disabled?(:auto_deploy_single_timer)
      puts 'Auto-deploy single timer disabled. Nothing to do here'
      next
    end

    ReleaseTools::Tasks::AutoDeploy::Timer.new.execute
  end

  # NOTE (rspeicher): We're in a transitional period for production health
  # status checks.
  #
  # Currently we're in a phase where everything is still going through this
  # single task, so we're still checking for specific environment variables,
  # but eventually these will be individual CI jobs triggering individual Rake
  # tasks.
  #
  # See https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1748
  desc "Validate production pre-checks"
  task :check_production do
    if ENV.key?('PRODUCTION_ISSUE_IID')
      ReleaseTools::Tasks::ProductionCheck::Incident.new.execute
    elsif ENV.key?('CHAT_CHANNEL')
      ReleaseTools::Tasks::ProductionCheck::Chatops.new.execute
    elsif ENV.key?('DEPLOYMENT_CHECK')
      ReleaseTools::Tasks::ProductionCheck::DeploymentStep.new.execute
    else
      ReleaseTools::Tasks::AutoDeploy::CheckProduction.new.execute
    end
  end

  desc "Perform Canary promotion pre-checks after a baking period"
  task :baking_time do
    ReleaseTools::Tasks::AutoDeploy::BakingTime.new.execute
  end

  desc "Perform gprd deployment pre-checks and post to Slack on failure"
  task :promotion_checks do
    ReleaseTools::Tasks::AutoDeploy::PromotionChecks.new.execute
  end

  desc "Authorizes a gprd deployment and post a message to the release issue"
  task :gprd_guard do
    ReleaseTools::Tasks::AutoDeploy::CheckProduction.new.execute
  end

  desc 'Cleans up old auto-deploy branches'
  task :cleanup do
    ReleaseTools::AutoDeploy::Cleanup.new.cleanup
  end

  desc 'Triggers a Deployer pipeline to a specific environment (DEPLOY_ENVIRONMENT) using a deployer branch (TRIGGER_REF)'
  task :deploy do
    ReleaseTools::Tasks::AutoDeploy::DeployTrigger
      .new(environment: ENV.fetch('DEPLOY_ENVIRONMENT', nil), ref: ENV.fetch('TRIGGER_REF', nil))
      .execute
  end

  desc 'Checks rollback availability for a package or environment'
  task :rollback_check do
    ReleaseTools::Tasks::AutoDeploy::RollbackCheck.new.execute
  end

  desc 'Sends a Slack notification about the status of a deployment'
  task :notify do
    ReleaseTools::AutoDeploy::CoordinatedPipeline::Notifier.new(
      pipeline_id: ENV.fetch('CI_PIPELINE_ID', nil),
      deploy_version: ENV.fetch('DEPLOY_VERSION', nil),
      environment: ENV.fetch('DEPLOY_ENVIRONMENT', nil),
      slack_channel: ENV.fetch('SLACK_CHANNEL', nil)
    ).execute
  end

  desc 'Checks if the package contains an older commit than the package already on DEPLOY_ENVIRONMENT'
  task :check_package do
    ReleaseTools::Tasks::AutoDeploy::CheckPackage.new.execute
  end

  namespace :wait do
    desc 'Wait for a tagged Omnibus pipeline to complete'
    task :omnibus do
      waiter = ReleaseTools::AutoDeploy::WaitForPackage.new(
        ReleaseTools::Project::OmnibusGitlab, 'omnibus-gitlab-ee'
      )

      if waiter.execute
        ReleaseTools.logger.info('Omnibus package built successfully')
      else
        exit 1
      end
    end

    desc 'Wait for a tagged CNG pipeline to complete'
    task :cng do
      project = ReleaseTools::Project::CNGImage

      waiter = ReleaseTools::AutoDeploy::WaitForPackage.new(
        project, project.metadata_project_name
      )

      if waiter.execute
        ReleaseTools.logger.info('CNG package built successfully')
      else
        exit 1
      end
    end

    desc 'Wait for a deployment to complete. The pipeline is identified by DEPLOY_PIPELINE_ID variable'
    task :deployment do
      client = ReleaseTools::GitlabOpsClient

      waiter = ReleaseTools::AutoDeploy::WaitForPipeline.new(
        client, ReleaseTools::Project::Deployer, ENV.fetch('DEPLOY_PIPELINE_ID', nil)
      )

      if waiter.execute
        ReleaseTools.logger.info('Deployment finished successfully')
      else
        exit 1
      end
    end
  end

  namespace :metrics do
    desc 'Tracks deployment start time'
    task :start_time do
      ReleaseTools::Metrics::CoordinatedDeployment::TrackStartTime.new
        .execute
    end

    desc 'Tracks deployment duration and number of deployments'
    task :end_time do
      ReleaseTools::Metrics::CoordinatedDeployment::Duration.new(
        deploy_version: ENV.fetch('DEPLOY_VERSION', nil),
        start_time: ENV.fetch('DEPLOY_START_TIME', nil)
      ).execute
    end

    desc 'Play manual jobs that generate metrics and traces'
    task :play_manual_jobs, [:pipeline_id] do |_t, args|
      pipeline_id = args[:pipeline_id].to_i

      ReleaseTools::Tasks::AutoDeploy::PlayManualJobs.new(pipeline_id).execute
    end
  end
end
