<!--

If you make any changes to this template in Release Tools, also make sure to
update any existing release issues (if necessary). If you've edited an active release issue
please consider making the same change in the template.

-->

## First steps

- [ ] Set the Due date on this issue with the planned Security publish date
- [ ] Disable Omnibus nightly builds by setting the schedules to inactive: https://dev.gitlab.org/gitlab/omnibus-gitlab/-/pipeline_schedules. This prevents us accidentally revealing vulnerabilities before the release.
- [ ] Post a message on the `#quality` Slack channel to notify the Quality team that a security release is in progress:
> Hello team, the security release has started (<link_to_this_issue>) and Omnibus nightly builds are now disabled. The GitLab ChatOps bot will post a notification to this channel when the security release is complete.
- [ ] Ensure that Canonical, Security and Build repositories are synced:
   ```sh
   # In Slack
   /chatops run mirror status
   ```
- [ ] Post a comment on https://gitlab.com/gitlab-jh/gitlab-jh-enablement/-/issues/112 to notify JiHU of the upcoming security release.
- [ ] Post a message on the #g_engineering_productivity channel to let them know that the secuirty release preperation has started. EP will use this information to quickly respond to pipeline failures to keep us unblocked
- [ ] Post a message on the `#g_runner` Slack channel to notify the Runner team that a security release is in progress and that it will be published on the due date.
- [ ] Verify if there are security fixes for projects under GitLab managed versioning model. If there are, adjust this issue [following the instructions](https://gitlab.com/gitlab-org/release/docs/-/blob/master/components/managed-versioning/security_release.md#release-manager-process).
<% if regular? -%>
  This is to synchronize the GitLab and the GitLab runner security release in case there is one planned.
- [ ] Modify the dates below to accurately reflect the plan of action.
- [ ] Verify pipelines on default and stable branches on GitLab are green:
   - [ ] [Default branch](https://gitlab.com/gitlab-org/gitlab/-/commits/<%= Project::GitlabEe.default_branch %>)
<% versions.each do |version| -%>
   - [ ] [<%= version.stable_branch(ee: true) %>](https://gitlab.com/gitlab-org/gitlab/-/commits/<%= version.stable_branch(ee: true) %>)
<% end %>
- [ ] Verify pipelines on the GitLab projects are green:
<% projects.each do |project| -%>
* **<%= project.metadata_project_name %>**
    - [ ] [Default branch](https://gitlab.com/<%= project.path %>/-/commits/<%= project.default_branch %>)
  <% versions.each do |version| -%>
  - [ ] [<%= version.stable_branch %>](https://gitlab.com/<%= project.path %>/-/commits/<%= version.stable_branch %>)
  <% end %>
<% end %>

## Early-merge phase

Up until the 27th, or one day before the Security Release due date

- Merge the merge requests targeting default branches
  ```
  # In Slack
  /chatops run release merge --security --default-branch
  ```

- [ ] Verify if a Gitaly security fix is included in the upcoming security release, if it is, follow the [How to deal with Gitaly security fixes](https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/security/how_to_handle_gitaly_security_merge_requests.md) guide.

## On the 25th (three days before due date)

- [ ] Check which security issues are not ready, and post a comment listing the issues that will be removed, why they will
  be removed (the reason returned by the `run release merge` command), by when they need to be ready (next day), and ping
  the authors. This serves as a warning to authors who weren't aware that their security issue was not in an acceptable
  state, and should hopefully result in less security issues needing to be unlinked.

## On the 27th (one day before due date)

If this date is on a weekend, do this work on the next working day.
<% else %>
## One day before the due date
<% end %>

- [ ] Determine the security release manager from the [schedule](https://about.gitlab.com/community/release-managers/). Look for the security release manager of the latest released monthly version
- [ ] Post the following message to `#sec-appsec` in Slack: `<security-release-manager> We are starting the [security release](<link to this issue>), aiming for release tomorrow. Please create a blog post MR on gitlab-org/security/www-gitlab-com.`
- [ ] Once the blog post MR has been created by the security release manager, add a link to it here: `https://gitlab.com/gitlab-org/security/www-gitlab-com/-/merge_requests/`
- [ ] Fetch the list of non-security MRs that will be included in these releases:
   ```sh
   /chatops run release pending_backports
   ```
   - [ ] Crosslink the list to `#sec-appsec` letting the security release manager these should be included in the blog post.
<% if regular? -%>
- [ ] Check that all MRs merged into the default branch have been deployed to production:
   ```sh
   # In Slack:
   /chatops run auto_deploy security_status
   ```

   NOTE: This only checks `gitlab-org/security/gitlab`. If other projects have security MRs you should verify those manually.
- [ ] Make sure to [execute the post-deploy migration pipeline] to ensure that all post-deploy migrations have been executed:
  `/chatops run post_deploy_migrations execute`
- [ ] Unlink any security issues that are not ready from the security release tracking issue (in `gitlab-org/gitlab`), and post a comment listing the issues that have been removed, why they have been removed (the reason returned by the `run release merge` command), and ping the authors. Make sure to complete this step before merging backports [to avoid picking up unexpected changes](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/2619)
- [ ] Merge backports and any other merge request pending:
   ```sh
   # In Slack:
   /chatops run release merge --security
   ```
- [ ] Ensure all Merge Requets associated with Implementation Issues with label `reduced backports` are merged
- [ ] If any merge requests could not be merged, investigate what needs to be done to resolve the issues. Do **not** proceed unless it has been determined safe to do so.
<% else -%>
- [ ] Merge critical security merge requests using the UI.
  - Enable "Squash commits" option when merging.
- [ ] Cherry-pick the security fixes into the auto-deploy branch that is running on production.
- [ ] Wait for the tests on the auto-deploy branch to pass. This ensures that when we tag, we tag the security commits; not older commits.
- [ ] Deploy all the fixes to production.
- [ ] Merge all backports into the appropriate stable branches (if required)
<% end -%>
- [ ] Ensure tests are green in CE and green in EE
   ```sh
   # In Slack:
   /chatops run release status --security
   ```
- [ ] If all the security issues have been deployed to production, consider tagging.

## On the Due Date

### Packaging

- [ ] Ensure tests are green in CE and green in EE
   ```sh
   # In Slack:
   /chatops run release status --security
   ```
**For the next task: Waiting between pipelines is necessary as they may otherwise fail to
concurrently push changes to the same project/branch.**

<% versions.each do |version| -%>
- [ ] Tag the <%= version.to_patch %> security release, and wait for the pipeline to finish: `/chatops run release tag --security <%= version.to_patch %>`
<% end %>


- [ ] Check that EE and CE packages are built:

  <% versions.each do |version| -%>
  - <%= version.to_patch %>: [EE packages](https://dev.gitlab.org/gitlab/omnibus-gitlab/-/commits/<%= version.to_omnibus(ee: true) %>) and [CE packages](https://dev.gitlab.org/gitlab/omnibus-gitlab/commits/<%= version.to_omnibus %>)
  <% end %>

### Deploy

- [ ] Verify that release.gitlab.net is running the latest patch version
  - Check in Slack `#announcements` channel
  - Go to https://release.gitlab.net/help

## Release

<% versions.each do |version| -%>
- [ ] Publish <%= version.to_patch %> via ChatOps:
   ```
   /chatops run publish --security <%= version %>
   ```
<% end %>

- [ ] Notify AppSec counterparts they can submit the blog post to `https://gitlab.com/gitlab-com/www-gitlab-com/`
- [ ] Verify that the `check-packages` job completes:
<% versions.each do |version| -%>
  - [ ] EE `check-packages` on [<%= version.to_omnibus(ee: true) %>](https://dev.gitlab.org/gitlab/omnibus-gitlab/-/commits/<%= version.to_omnibus(ee: true) %>)
  - [ ] CE `check-packages` on [<%= version.to_omnibus(ee: false) %>](https://dev.gitlab.org/gitlab/omnibus-gitlab/-/commits/<%= version.to_omnibus(ee: false) %>)
<% end %>
- [ ] Verify that Docker images appear on `hub.docker.com`: [EE](https://hub.docker.com/r/gitlab/gitlab-ee/tags) / [CE](https://hub.docker.com/r/gitlab/gitlab-ce/tags)
- [ ] Deploy the blog post
- [ ] Create the versions:
<% versions.each do |version| -%>
  - [ ] Create `<%= version %>` version on [version.gitlab.com](https://version.gitlab.com/versions/new?version=<%= version %>). **Be sure to mark it as a security release.**
<% end %>

### Final steps

- [ ] Sync default branches for GitLab, GitLab Foss, Omnibus GitLab and Gitaly, via ChatOps:
   ```sh
   # In Slack
   /chatops run release sync_remotes --security
   ```

- [ ] Verify all remotes are synced:

   ```sh
   # In Slack
   /chatops run mirror status
   ```

   If conflicts are found, [manual intervention will be needed to sync the repositories](https://gitlab.com/gitlab-org/release/docs/-/blob/master/runbooks/manually-sync-release-tag.md).

- [ ] Close the security implementation issues
   ```sh
   # In Slack
   /chatops run release close_issues --security
   ```

- [ ] Notify engineers the security release is out (`blog post link` needs to be replaced with the actual link):
   ```
   /chatops run notify ":mega: <%= informative_title %>: <%= versions_title %> has just been released: <blog post link>! Share this release blog post with your network to ensure broader visibility across our community."
   ```

- [ ] Enable Omnibus nightly builds by setting the schedules to active https://dev.gitlab.org/gitlab/omnibus-gitlab/-/pipeline_schedules
- [ ] In case it was disabled, enable the [Gitaly update task](https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules/112/edit).

<% if regular? -%>
- [ ] Close the old security release tracking issue and create a new one:
   ```sh
   # In Slack
   /chatops run release tracking_issue --security
   ```
- [ ] Ping the [next set of release managers] on the [upcoming security release] issue and ask them to set the intended security release due date. If needed, suggest a possible due date based on the current release activities.
- [ ] Check all new [tags have synced to Canonical](https://gitlab.com/gitlab-org/gitlab/-/tags)
- [ ] Link the new security release tracking issue on the topic of the `#releases` channel, next to `Next Security Release`.
<% else -%>
- [ ] Close the critical security tracking issue
<% end -%>

[execute the post-deploy migration pipeline]: https://gitlab.com/gitlab-org/release/docs/-/tree/master/general/post_deploy_migration#how-to-execute-post-deploy-migrations
[next set of release managers]: https://about.gitlab.com/community/release-managers/
[upcoming security release]: https://gitlab.com/gitlab-org/gitlab/-/issues/?label_name%5B%5D=upcoming%20security%20release
