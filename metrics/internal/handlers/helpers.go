package handlers

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"gitlab.com/gitlab-org/release-tools/metrics/internal/logger"
	"gitlab.com/gitlab-org/release-tools/metrics/internal/metrics"
)

func route(metric metrics.Metadata) string {
	return fmt.Sprintf("/%s_%s", metric.Subsystem(), metric.Name())
}

func getValue(r *http.Request) (float64, error) {
	raw := r.FormValue("value")

	return strconv.ParseFloat(raw, 64)
}

func getLabels(r *http.Request) []string {
	labels := r.FormValue("labels")

	return strings.Split(labels, ",")
}

func badRequest(w http.ResponseWriter, r *http.Request, reason string) {
	w.WriteHeader(http.StatusBadRequest)
	answer(w, r, reason)
}

func answer(w http.ResponseWriter, r *http.Request, message string) {
	_, err := w.Write([]byte(message))
	if err != nil {
		log := logger.Get(r)
		log.WithError(err).Error("Cannot write the HTTP answer")
	}
}
