package handlers

import (
	"testing"

	"github.com/stretchr/testify/require"
)

type mockCounter struct {
	mockMetric
}

func (m *mockCounter) Inc(labels ...string) {
	m.value += 1
}

func (m *mockCounter) Add(value float64, labels ...string) {
	m.value = value
}

func TestCounterVecIncHandler(t *testing.T) {
	meta := &mockCounter{}
	meta.expectedLabels = []string{"green"}
	handler := NewCounter(meta).(*counter)

	req, err := meta.apiRequest("inc", "")
	require.NoError(t, err)

	meta.testRequest(t, req, handler, 1)

	resetReq, err := meta.apiReset()
	if err != nil {
		t.Fatal(err)
	}

	meta.testRequest(t, resetReq, handler, 0)
}

func TestCounterVecAddHandler(t *testing.T) {
	meta := &mockCounter{}
	meta.expectedLabels = []string{"green"}
	handler := NewCounter(meta).(*counter)

	req, err := meta.apiRequest("add", "10.0")
	require.NoError(t, err)

	meta.testRequest(t, req, handler, 10)

	resetReq, err := meta.apiReset()
	if err != nil {
		t.Fatal(err)
	}

	meta.testRequest(t, resetReq, handler, 0)
}
